package com.ummo.prod.agent.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ummo.prod.agent.room.entities.Enqueue;

import java.util.List;

@Dao
public interface EnqueueDao {
    //Create
    @Insert
    void insertEnqueue(Enqueue enqueue);

    //Read
    @Query("select * from enqueue")
    List<Enqueue> loadEnqueues();

    //Update
    @Update
    void updateEnqueues(Enqueue enqueue);

    //Delete
    @Delete
    void deleteEnqueue(Enqueue enqueue);
}
