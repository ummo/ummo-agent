package com.ummo.prod.agent.model;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ummo.prod.agent.UmmoAgent;

/**
 * Created by mosaic on 7/24/16.
 */
public class UmmoAgentWrapper {
    private  String email;
    private String uname;
    private String cell;
    private String UmmoId;
    private boolean registered=false;
    public UmmoAgent ui;

    public String getUname() {
        return uname;
    }

    public String getUmmoId() {
        return UmmoId;
    }

    public String getEmail() {
        return email;
    }

    public String getCell() {
        return cell;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setUmmoId(String ummoId) {
        UmmoId = ummoId;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public UmmoAgentWrapper(UmmoAgent app){
        ui=app;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ui);
        setUmmoId(sp.getString("ummoid",""));
        setUname(sp.getString("uname",""));
        setCell(sp.getString("cell",""));
        setEmail(sp.getString("mail",""));
        setRegistered(sp.getBoolean("registered",false));
    }

    public void save(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ui);
        sp.edit().putString("uname",getUname()).putBoolean("registered",isRegistered()).putString("cell",getCell()).putString("ummoid",getUmmoId()).putString("mail",getEmail()).apply();
    }
}
