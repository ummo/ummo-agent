package com.ummo.prod.agent.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ummo.prod.agent.room.entities.Agent;

import org.json.JSONObject;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface AgentDao {
    //Create
    @Insert(onConflict = REPLACE)
    void createAgent(Agent agent);

    //Read
    @Query("select * from agent")
    List<Agent> loadAgent();

    //Update
    @Update
    void updateAgent(Agent agent);

    //Delete
    @Query("delete from agent where agent_email = :email and agent_id = :id")
    void deleteAgent(String email, String id);
}
