package com.ummo.prod.agent.ummoAPI;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


import com.ummo.prod.agent.AlarmReciever;
import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.ummoAPI.interfaces.DataReadyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import io.socket.client.Socket;

/**
 * Created by mosaic on 5/22/17.
 */

public class BookingWrapper {
    private static List<BookingWrapper> bookings = new ArrayList<>();
    private String _id;
    private String name;
    private String cell;
    private Calendar start;
    private Calendar end;
    private String service;
    public static BookingWrapper temp;
    private static Socket socket = UmmoAgent.getApp().getSocket();

    private static DataReadyCallBack onLoadedBookings = null;
    private static DataReadyCallBack onBookingCreated = null;

    public static void setOnLoadedBookings(DataReadyCallBack onLoadedBookings) {
        BookingWrapper.onLoadedBookings = onLoadedBookings;
        if(bookings.size()>0) BookingWrapper.onLoadedBookings.exec(bookings);
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getCell() {
        return cell;
    }

    public static List<BookingWrapper> getBookings() {
        return bookings;
    }

    public static void setBookings(List<BookingWrapper> bookings) {
        BookingWrapper.bookings = bookings;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getEnd() {
        return end;
    }

    public void setEnd(Calendar end) {
        this.end = end;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public BookingWrapper(){

    }

    private int getHash(){
        if(_id==null)return 0;
        int _hash = 7;
        for (int i = _id.length()-1; i > _id.length()-5; i--) {
            _hash = _hash*31 + _id.charAt(i);
        }
       // hash=_hash;
        return _hash;
    }


    public void setAlarm(){
        AlarmManager alarmMgr;
        AlarmManager alarmMgr2;
        PendingIntent alarmIntent;
        PendingIntent alarmIntent2;
        alarmMgr = (AlarmManager) UmmoAgent.getApp().getSystemService(Context.ALARM_SERVICE);
        alarmMgr2 = (AlarmManager) UmmoAgent.getApp().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(UmmoAgent.getApp(), AlarmReciever.class);
        intent.putExtra("service",getService());
        intent.putExtra("period"," about two hours");
        intent.putExtra("id",get_id());
        intent.putExtra("hash",getHash());

        Intent intent2 = new Intent(UmmoAgent.getApp(), AlarmReciever.class);
        intent2.putExtra("service",getService());
        intent2.putExtra("period"," a day");
        intent2.putExtra("id",get_id());
        intent2.putExtra("hash",getHash()+2);
        alarmIntent = PendingIntent.getBroadcast(UmmoAgent.getApp(),getHash(),intent,PendingIntent.FLAG_UPDATE_CURRENT);
        alarmIntent2 = PendingIntent.getBroadcast(UmmoAgent.getApp(),getHash()+2,intent2,PendingIntent.FLAG_UPDATE_CURRENT);
        long two_hour = start.getTimeInMillis()-2*60*60*1000;
        long day = start.getTimeInMillis()-24*60*60*1000;
        long now = Calendar.getInstance().getTime().getTime();
        if(now>two_hour)return;
        alarmMgr.set(AlarmManager.RTC_WAKEUP,two_hour,alarmIntent);
        if (now>day)return;
        alarmMgr2.set(AlarmManager.RTC_WAKEUP,day,alarmIntent2);
    }
    
    public BookingWrapper(JSONObject obj){
        try {
            set_id(obj.getString("_id"));

            setService(obj.getString("service"));
            if(obj.has("user")){
                setCell(obj.getJSONObject("user").getString("cell"));
                setName(obj.getJSONObject("user").getString("name"));
                Log.e("BOOKIGCONST","QMOD");
            }else {
                Log.e("BOOKIGCONST","QMOD");
                setCell(obj.getJSONObject("booker").getString("cell"));
                setName(obj.getJSONObject("booker").getJSONObject("fullName").getString("firstName"));
            }
            setStart(Calendar.getInstance());
            setEnd(Calendar.getInstance());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getDefault());

            try {
                start.setTime(simpleDateFormat.parse(obj.getString("start")));
                end.setTime(simpleDateFormat.parse(obj.getString("end")));
                setAlarm();
                Log.e("BOOKING_TIMES",start.getTime().toString()+" => "+getEnd().getTime().toString());
            }catch (ParseException pse){
                Log.e("PARSERR",getClass().getCanonicalName()+pse.toString());
            }

        }catch (JSONException jse){
            Log.e("ERRNEWBOOKING",jse.toString()+obj.toString());
        }
    }



    public static void loadBookings(){
        Log.e("LOADBOOKINGS","LOADING");
        SocketClass.addDefaultEventHandler("agent-bookings", new SocketClass.Response() {
            @Override
            public Object ready(Object val) {
                Log.e("AGENT_BOOKINGS",val.toString());
                JSONArray array = (JSONArray)val;
                bookings.clear();
                for(int i =0;i<array.length();i++){
                    try {
                        //Log.e("")
                        bookings.add(new BookingWrapper(array.getJSONObject(i)));
                    }catch (JSONException jse){
                        Log.e("ERRLOADBOOKINGS",jse.toString());
                    }
                }
                if (onLoadedBookings!=null) onLoadedBookings.exec(bookings);
                return null;
            }
        });
        SocketClass.emit("agent-bookings", UmmoAgent.getApp().getUmmoId(), new SocketClass.Response() {
            @Override
            public Object ready(Object val) {
                Log.e("AGENT_BOOKINGS",val.toString());
                JSONArray array = (JSONArray)val;
                bookings.clear();
                for(int i =0;i<array.length();i++){
                    try {
                        bookings.add(new BookingWrapper(array.getJSONObject(i)));
                    }catch (JSONException jse){
                        Log.e("ERRLOADBOOKINGS",jse.toString());
                    }
                }
                if (onLoadedBookings!=null) onLoadedBookings.exec(bookings);
                return null;
            }
        });
    }

    public static void setOnBookingCreated(DataReadyCallBack onBookingCreated) {
        BookingWrapper.onBookingCreated = onBookingCreated;
    }

    public void setBookingDone(){
        SocketClass.emit("booking-done", _id, new SocketClass.Response() {
            @Override
            public Object ready(Object val) {
                Log.e("BookingWrapper","Booking Done");
                return null;
            }
        });
    }

    public static void createBooking(String name, String cell, Calendar start, Calendar end, String service){
        Log.e("Create Booking"," Start "+start.getTimeInMillis()+" End "+end.getTime().toString());
        JSONObject object = new JSONObject();
        JSONObject user = new JSONObject();
        Calendar _start = start;
        Calendar _end = end;
        int offset =_end.getTimeZone().getRawOffset();
        //Log.e("START",start.getTimeZone().toString()+" , "+_start.getTime().toString());
        try{
            user.put("name",name);
            user.put("cell",cell);
            object.put("user",user);
            object.put("service",service);
            object.put("agent", UmmoAgent.getApp().getUmmoId());
            object.put("start",_start.getTimeInMillis()+offset);
            object.put("end",_end.getTimeInMillis()+offset);
            BookingWrapper bookingWrapper = new BookingWrapper();
            bookingWrapper.setName(name);
            bookingWrapper.setCell(cell);
            bookingWrapper.set_id("");
            bookingWrapper.setStart(_start);
            bookingWrapper.setEnd(_end);
            temp=bookingWrapper;
            //socket.emit("create-booking-mod",object);
            SocketClass.emit("create-booking-mod", object, new SocketClass.Response() {
                @Override
                public Object ready(Object val) {
                    try {
                        JSONObject _val = new JSONObject(val.toString());
                        if(temp.get_id().equals(_val.getString("_id"))){
                            Log.e("BookingWrapper","Booking Already recieved");
                        }else {
                            Log.e("BookingWrapper","Booking Created "+val.toString());
                            temp = new BookingWrapper(_val);

                        }
                    }catch (JSONException jse){
                        Log.e("JSONERR",val.toString());
                    }
                    if(onBookingCreated!=null)onBookingCreated.exec(val);
                    return null;
                }
            });
        }catch (JSONException jse){
            Log.e("ERRCREATEBOOKING",jse.toString());
        }
    }

    public static void saveTempBooking(){
        bookings.add(temp);
    }


}
