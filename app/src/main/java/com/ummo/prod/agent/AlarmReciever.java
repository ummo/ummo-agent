package com.ummo.prod.agent;

/**
 * Created by mosaic on 5/25/17.
 */


import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class AlarmReciever extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String _id = intent.getStringExtra("_id");
        int hash = intent.getIntExtra("hash",100);
        String _service = intent.getStringExtra("service");
        String period = intent.getStringExtra("period");
        Log.e("PArAMS", "id: "+_id+" service: "+_service+" period: "+period);
        UmmoAgent.getApp().bookingCloseNotification(_id,_service,period,hash);
    }
}
