package com.ummo.prod.agent.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ummo.prod.agent.room.entities.Served;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ServedDao {
    //Create
    @Insert(onConflict = REPLACE)
    void insertServed(Served served);

    //Read
    @Query("select * from served")
    List<Served> loadServed();

    //Update
    @Update
    void updateServed(Served served);

    //Delete
    @Delete
    void deleteServed(Served served);
}
