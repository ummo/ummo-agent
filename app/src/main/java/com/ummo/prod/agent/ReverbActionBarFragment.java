package com.ummo.prod.agent;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ummo.prod.agent.R;

import com.facebook.accountkit.ui.LoginFlowState;

/**
 * Created by barnes on 11/14/16.
 */

public class ReverbActionBarFragment extends Fragment {
    private static final String STATE_KEY = "state";

    private LoginFlowState state = LoginFlowState.NONE;

    public void setState(final LoginFlowState state) {
        if (state == null) {
            return;
        }
        this.state = state;
        updateTitleView(getView());
    }

    @Nullable
    @Override
    public View onCreateView(
            final LayoutInflater inflater,
            final ViewGroup container,
            final Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            final String stateString = savedInstanceState.getString(STATE_KEY);
            state = stateString == null ? state : LoginFlowState.valueOf(stateString);
        }

        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_reverb_action_bar, container, false);
        }
        if (view == null) {
            return null;
        }
        updateTitleView(view);
        return view;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(STATE_KEY, state.name());
    }

    private void updateTitleView(@Nullable final View view) {
        if (view == null || state == null) {
            return;
        }
        final TextView titleView = (TextView) view.findViewById(R.id.title_view);
        if (titleView != null) {
            switch (state) {
                case PHONE_NUMBER_INPUT:
                    titleView.setText(R.string.reverb_title_phone_number_input);
                    break;
                case EMAIL_INPUT:
                    titleView.setText(R.string.reverb_title_email_input);
                    break;
                case SENDING_CODE:
                    titleView.setText(R.string.reverb_title_sending_code);
                    break;
                case SENT_CODE:
                    titleView.setText(R.string.reverb_title_sent_code);
                    break;
                case CODE_INPUT:
                    titleView.setText(R.string.reverb_title_code_input);
                    break;
                case EMAIL_VERIFY:
                    titleView.setText(R.string.reverb_title_email_verify);
                    break;
                case VERIFYING_CODE:
                    titleView.setText(R.string.reverb_title_verifying_code);
                    break;
                case VERIFIED:
                    titleView.setText(R.string.reverb_title_verified);
                    break;
                case RESEND:
                    titleView.setText(R.string.reverb_title_resend);
                    break;
                case ERROR:
                    titleView.setText(R.string.reverb_title_error);
                    break;
            }
        }
    }
}
