package com.ummo.prod.agent.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Agent{
    @PrimaryKey
    @NonNull
    public String agent_id;

    public String agent_email;

    public String agent_code;
/*
    public String agent_name;

    public String agent_surname;*/
}