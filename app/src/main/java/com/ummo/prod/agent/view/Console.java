package com.ummo.prod.agent.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.room.AppDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import io.doorbell.android.Doorbell;

/**
 * Created by barnes on 11/9/16.
 **/

public class Console extends AppCompatActivity {
    ImageButton monitor;
    RelativeLayout monitorRL;
    RelativeLayout manageRL;
    ImageButton manager;
    ImageButton booking;
    ImageButton logout;
    private String TAG = "Console";
    String sharedPrefFile = "com.ummo.prod.agent";
    private AppDatabase appDatabase;
    private UmmoAgent ummoAgent = new UmmoAgent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.console);
        Toolbar consoleToolBar = findViewById(R.id.console_tool_bar);
        setSupportActionBar(consoleToolBar);
        setTitle("Ummo Agent");

        String signedInAgent = UmmoAgent.getApp().getUname();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //window.setStatusBarColor(ContextCompat.getColor(this, R.color.ummo));
        }

        if (getSupportActionBar() != null){
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setSubtitle("Signed in as: " + signedInAgent);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        String retrievedQueues = sharedPreferences.getString("SAVED_QUEUE_JOINS", "");
        Log.e(TAG+"", "Retrieved Queue -> "+retrievedQueues);

        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);

        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);

        monitor = findViewById(R.id.monitor);
        monitorRL = findViewById(R.id.monitor_layout);
        monitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Mixpanel Tracking of Agent Sign-Up
                try{
                    JSONObject props = new JSONObject();
                    props.put("monitor_service_bt","clicked");
                    mixpanelAPI.track("Monitor Service",props);
                } catch (JSONException e) {
                    Log.e("Console Activity", "Unable to add properties to JSONObject", e);
                }
                startActivity(new Intent(Console.this, MonitorTabsActivity.class));
            }
        });
        /*monitorRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup viewGroup = (ViewGroup) v;
                for (int i = 0; i < viewGroup.getChildCount(); i++) {

                    View viewChild = viewGroup .getChildAt(i);
                    viewChild.setPressed(true);
                    Intent intent = new Intent(Console.this, AnimateToolbar.class);
                    startActivity(intent);

                }
            }
        });*/

        manager = findViewById(R.id.manageButton);
        manageRL = findViewById(R.id.manage_layout);
        manager.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //Mixpanel Tracking of Agent Sign-Up
                try{
                    JSONObject props = new JSONObject();
                    props.put("manage_queue_bt","clicked");
                    mixpanelAPI.track("Manage Queue",props);
                } catch (JSONException e) {
                    Log.e("Console.java", "Unable to add properties to JSONObject", e);
                }
                Intent swipe_cards = new Intent(Console.this, ManageQueue.class);
                startActivity(swipe_cards);
            }
        });
        /*manageRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewGroup viewGroup = (ViewGroup) v;
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View viewChild = viewGroup .getChildAt(i);
                    viewChild.setPressed(true);
                    Intent intent = new Intent(Console.this, ManageQueue.class);
                    startActivity(intent);
                }
            }
        });*/

        booking = findViewById(R.id.manageBookingButton);
        booking.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                //Mixpanel Tracking of Agent Sign-Up
                try{
                    JSONObject props = new JSONObject();
                    props.put("manage_booking_bt","clicked");
                    mixpanelAPI.track("Manage Bookings",props);
                } catch (JSONException e) {
                    Log.e("Console Activity", "Unable to add properties to JSONObject", e);
                }
                Intent booking = new Intent(Console.this, DiscoverActivity.class);
                startActivity(booking);
            }
        });

        logout = findViewById(R.id.messagesButton);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Mixpanel Tracking of Agent Sign-Out
                try{
                    JSONObject props = new JSONObject();
                    props.put("logout_bt","clicked");
                    mixpanelAPI.track("Logged Out",props);
                } catch (JSONException e) {
                    Log.e("Console Activity", "Unable to add properties to JSONObject", e);
                }
                UmmoAgent.getApp().logout();
                startActivity(new Intent(Console.this,SplashScreen.class));
                finish();
            }
        });
        //Typeface font = Typeface.createFromAsset( getAssets(), "fontawesome-webfont.ttf" );
        //registerButton = (Button) findViewById(R.id.registerbutton);
        //myWalletButton = (Button) findViewById(R.id.myWalletButton);
        //myWalletButton.setBackgroundResource(R.drawable.ic_wallet);
        //registerButton.setTypeface(font);
    }

    public void executeMonitorClick (View view) {
        //Mixpanel Tracking of Agent Sign-Up
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        try{
            JSONObject props = new JSONObject();
            props.put("monitor_service_rl","clicked");
            mixpanelAPI.track("Monitor Service",props);
        } catch (JSONException e) {
            Log.e("Console Activity", "Unable to add properties to JSONObject", e);
        }
        startActivity(new Intent(Console.this, MonitorTabsActivity.class));
    }

    public void executeManageQClick (View view) {
        //Mixpanel Tracking of Agent Sign-Up
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        try{
            JSONObject props = new JSONObject();
            props.put("manage_queue_rl","clicked");
            mixpanelAPI.track("Manage Queue",props);
        } catch (JSONException e) {
            Log.e("Console Activity", "Unable to add properties to JSONObject", e);
        }
        startActivity(new Intent (Console.this, ManageQueue.class));
    }

    public void executeManageBookingClick (View view) {
        //Mixpanel Tracking of Agent Sign-Up
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        try{
            JSONObject props = new JSONObject();
            props.put("manage_booking_rl","clicked");
            mixpanelAPI.track("Manage Bookings",props);
        } catch (JSONException e) {
            Log.e("Console Activity", "Unable to add properties to JSONObject", e);
        }
        startActivity(new Intent (Console.this, DiscoverActivity.class));
    }

    public void executeLogOutClick (View view) {
        //Mixpanel Tracking of Agent Sign-Up
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        try{
            JSONObject props = new JSONObject();
            props.put("logout_rl","clicked");
            mixpanelAPI.track("Logged Out",props);
        } catch (JSONException e) {
            Log.e("Console Activity", "Unable to add properties to JSONObject", e);
        }
        UmmoAgent.getApp().logout();
        startActivity(new Intent (Console.this, SplashScreen.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_support, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.supportDialog:
                confirmFeedback();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void confirmFeedback() {
        Log.e(TAG+" confirm", "Support tapped");
        //DialogFragment newFragment = new FeedbackFragment();
        //newFragment.show(getSupportFragmentManager(), "feedback");
        Doorbell doorbell = new Doorbell(this, 7985, getString(R.string.doorbellPrivateKey));
        doorbell.setTitle("Ummo Support")
                .setName(UmmoAgent.getApp().getUname())
                .addProperty("Contact", UmmoAgent.getApp().getEmail())
                .addProperty("User-Name", UmmoAgent.getApp().getUname())
                .setEmailFieldVisibility(View.VISIBLE)
                .setEmailHint("Email (optional)")
                .setMessageHint("Let us know how we can help...")
                .setNegativeButtonText("Maybe Later")
                .setPoweredByVisibility(View.GONE)
                .captureScreenshot()
                .setIcon(R.mipmap.ummo_agent_new)
                .setCancelable(false)
                //.create()
                .show();
    }
}