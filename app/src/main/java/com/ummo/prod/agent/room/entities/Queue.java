package com.ummo.prod.agent.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Queue {
    @PrimaryKey
    @NonNull
    public String member_id; //AKA User Contact

    public String joinTime;

    public String user_name;

    public String num_code;

    public int position;
}
