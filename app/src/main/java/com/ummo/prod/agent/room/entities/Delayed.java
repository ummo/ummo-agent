package com.ummo.prod.agent.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.sql.Timestamp;

@Entity
public class Delayed {
    @PrimaryKey
    @NonNull
    public String user_contact;

    public String joinTime;

    //TODO:Convert to Timestamp
    public String delayed_time;
}