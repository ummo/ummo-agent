package com.ummo.prod.agent.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.ummo.prod.agent.room.dao.AgentDao;
import com.ummo.prod.agent.room.dao.DelayedDao;
import com.ummo.prod.agent.room.dao.EnqueueDao;
import com.ummo.prod.agent.room.dao.QueueDao;
import com.ummo.prod.agent.room.dao.ServedDao;
import com.ummo.prod.agent.room.entities.Agent;
import com.ummo.prod.agent.room.entities.Delayed;
import com.ummo.prod.agent.room.entities.Enqueue;
import com.ummo.prod.agent.room.entities.Queue;
import com.ummo.prod.agent.room.entities.Served;

@Database(entities = {Agent.class, Enqueue.class, Served.class, Delayed.class, Queue.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{
    private static AppDatabase INSTANCE;

    public abstract AgentDao agentDao();

    public abstract EnqueueDao enqueueDao();

    public abstract QueueDao queueDao();

    public abstract ServedDao servedDao();

    public abstract DelayedDao delayedDao();

    public static AppDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), AppDatabase.class)
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }
}