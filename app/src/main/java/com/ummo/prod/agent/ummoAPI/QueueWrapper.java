package com.ummo.prod.agent.ummoAPI;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.ummoAPI.interfaces.DataReadyCallBack;

import java.util.ArrayList;
import java.util.List;

import io.socket.emitter.Emitter;

/**
 * Created by mosaic on 5/4/17.
 **/

public class QueueWrapper {
    private String qName;
    private String qActive;
    private  String qRequirements;
    private String _id;
    private int ttdq;
    private int length;
    private String location;
    public static QueueWrapper managedQueue=null;
    private static DataReadyCallBack selectedQCallback=null;
    private static DataReadyCallBack joinCallBack;
    private static String TAG = "QueueWrapper";

    public static List<QueueJoinWrapper> unsent = new ArrayList<>();

    public void setSelectedQCallback(DataReadyCallBack _cb){
        selectedQCallback = _cb;
    }

    public static  void setJoinCallBack(DataReadyCallBack jc){
        joinCallBack=jc;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String get_id() {
        return _id;
    }

    public static void loadQueue(String q){
        UmmoAgent.getApp().getSocket().emit("get_queue_data",q);
        UmmoAgent.getApp().getSocket().on("get_queue_data", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG+" Queue Data -> ",args[0].toString());

                try {
                    JSONObject object = (JSONObject)args[0];
                    JSONArray array = object.getJSONArray("queueings");
                   // QueueJoinWrapper.setData(array);
                }catch (JSONException jse){
                    Log.e("Retrieving JOINS_ERR",jse.toString());
                }
                managedQueue=new QueueWrapper((JSONObject)args[0]);
                //Log.e("Managed Queue",managedQueue.get_id());
                if(selectedQCallback!=null)
                    selectedQCallback.exec(managedQueue);
            }
        });
    }

    public static void deQueue(){
        if (managedQueue==null)
            return;

        Log.e(TAG+" deQueue","IS NULL ->"+managedQueue.get_id());
        UmmoAgent.getApp().getSocket().emit("dequeue",managedQueue.get_id());

        UmmoAgent.getApp().getSocket().on("dequeue", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                QueueWrapper.loadData();
                Log.e(TAG+" Dequeue -> ",args[0].toString());

                /*String dequeueContact;
                JSONArray dequeueMembers;
                try {
                    JSONObject dequeueMember = new JSONObject(args[0].toString());
                    dequeueContact = dequeueMember.getString("userCell");
                    Log.e(TAG+" Dequeue", "Contact->"+dequeueContact);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG+" Dequeue", "Err... Error->",e);
                }*/
                /*try {
                    dequeueMembers = new JSONArray(args[0].toString());
                    Log.e(TAG+" Dequeue", "JSON Array->"+dequeueMembers);

                    for (int i=0; i<dequeueMembers.length(); i++){
                        if (dequeueMembers.getJSONObject(i).get("possition").equals(0)) {
                            dequeueMember = dequeueMembers.getJSONObject(i);
                            dequeueContact = dequeueMember.getString("userCell");
                            Log.e(TAG+" Dequeue", "Contact->"+dequeueContact);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG+" Dequeue", "Err... Error->",e);
                }*/
            }
        });
    }

    public static void loadData(){
        UmmoAgent.getApp().getSocket().emit("load_agent_queue_joins", UmmoAgent.getApp());
        UmmoAgent.getApp().getSocket().off("agent_queue_joins");
        UmmoAgent.getApp().getSocket().on("agent_queue_joins", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG+" Queue Joins -> ",args[0].toString());
                QueueJoinWrapper.setData((JSONArray)args[0]);
            }
        });
    }

    public static void joinSelectedQueue(){
       /* String cell = UmmoAgent.getApp().getCellNumb();
        try{
            JSONObject object = new JSONObject();
            object.put("que",selectedQueue.get_id());
            object.put("_cell",qman.getQman().getCellNumb());
            qman.getQman().getSocket().emit("join_selected_queue",object);
            qman.getQman().getSocket().on("queue_joined", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("QUEUEJOIN",args[0].toString());
                }
            });
        }
        catch (JSONException jse){
            Log.e("JOINERR",jse.toString());
        }*/

    }

    public static void qModJoin(String name,String cell){
        try {
            JSONObject object = new JSONObject();

            if (cell.length()==8) cell="+268"+cell;
            if (cell.length()==11) cell="+"+cell;
            object.put("cell",cell);
            object.put("queue",managedQueue.get_id());
            object.put("name",name);
            Log.e(TAG+" qModJoin",object.toString());
            UmmoAgent.getApp().getSocket().emit("qmod_join",object);
            UmmoAgent.getApp().getSocket().on("qmod_join",
                    new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    QueueJoinWrapper.loadData();

                    Log.e(TAG+" Qmod -> ","JOIN SUCCESS");
                }
            });

            //QueueJoinWrapper join = new QueueJoinWrapper(name,cell);
            //unsent.add(join);
            //object.put("queue",unsent);

        }catch (JSONException jse){
            Log.e("EQMODJOIN",jse.toString());
        }
    }

    private void set_id(String _id) {
        this._id = _id;
    }

    public void setqActive(String qActive) {
        this.qActive = qActive;
    }

    private void setqName(String qName) {
        this.qName = qName;
    }

    public String getqActive() {
        return qActive;
    }

    public String getqName() {
        return qName;
    }

    public String getqRequirements() {
        return qRequirements;
    }

    private void setqRequirements(String qRequirements) {
        this.qRequirements = qRequirements;
    }

    public int getTtdq() {
        return ttdq;
    }

    private void setTtdq(int ttdq) {
        this.ttdq = ttdq;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    private QueueWrapper(JSONObject _object){
        //Log.e("CREATING QUEEU",object.toString());
        try{
            JSONObject object=_object.has("data")?_object.getJSONObject("data"):_object;
            setqName(object.getString("qName"));
            setqRequirements(object.getString("qRequirements"));
            set_id(object.getString("_id"));
            setLocation(object.getString("location"));
            if(object.has("ttdq"))setTtdq(object.getInt("ttdq"));
            if(object.has("length"))setLength(object.getInt("length"));
        }catch (JSONException jse){
            Log.e(TAG+" Error Creating Queue->",jse.toString());
        }
    }
}
