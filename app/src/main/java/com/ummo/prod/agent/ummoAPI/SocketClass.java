package com.ummo.prod.agent.ummoAPI;

import android.util.Log;

import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.UmmoAgent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;


/**
 * Created by mosaic on 6/4/17.
 */

public class SocketClass {
    private static Socket socket= UmmoAgent.getApp().getSocket();
    private static boolean connected=false;

    public interface Response{
        public Object ready(Object val);
    }

    private static HashMap<String,Request> requestQueue = new HashMap<>();
    private static HashMap<String,Request> defaultEventHandlers = new HashMap<>();

    public static void addDefaultEventHandler(String key,Response respn){
        if(defaultEventHandlers.containsKey(key))return;
        defaultEventHandlers.put(key,new Request(key,null,respn));
    }

    public static void emit(final String key,Object value,Response repn){
        socket.emit(key,value);
        Log.e("EMIT",value.toString());
        socket.on(key, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("SOCKET_CLASS","ARGS[0] "+args[0]);
                Log.e("KEY ",key);
                Request request=requestQueue.remove(key);
                if(request==null){
                    //Log.e("SOCKETCLASS")
                    Request rq = defaultEventHandlers.get(key);
                    if (rq!=null){
                        rq.exec(args[0]);
                    }else {
                        Log.e("SOCKETCLASSERR","No event handler found for "+key);
                    }
                    return;
                }
                request.exec(args[0]);
            }
        });
        requestQueue.put(key,new Request(key,value,repn));
    }

    public static void init(){

        socket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Transport transport = (Transport)args[0];

                transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        @SuppressWarnings("unchecked")
                        Map<String,List<String>> headers = (Map<String, List<String>>)args[0];
                        // modify request headers
                        //Log.e("COOKIE",UmmoAgent.getApp().getUmmoId());
                        //headers.put("Cookie",  Arrays.asList("usr=mosaic;uid="+UmmoAgent.getApp().getUmmoId()));
                    }
                });

                transport.on(Transport.EVENT_RESPONSE_HEADERS, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        @SuppressWarnings("unchecked")
                        Map<String, List<String>> headers = (Map<String, List<String>>)args[0];
                        // access response headers
                        // String cookie = headers.get("Set-Cookie").toString();
                        //Log.e("SET COOKIE",cookie);
                    }
                });
            }
        });
           // socket.connect();
            socket.on(Socket.EVENT_RECONNECT_ATTEMPT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_RECONNECT_ATTEMPT");
                }
            });
            socket.on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_RECONNECT");
                }
            });
            socket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_ERROR "+args[0].toString());
                }
            });
            socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(!connected)return;
                    connected=false;
                    Log.e("SOCKET","EVENT_DISCONNECT");
                }
            });
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_CONNECT_TIMEOUT");
                }
            });
            socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","EVENT_CONNECT_ERROR"+args[0]);
                }
            });
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (connected) return;
                    Log.e("SOCKET","EVENT_CONNECT");
                    connected=true;
                    Object[] keys = requestQueue.keySet().toArray();
                    for (int i =0;i<keys.length;i++){
                        Request r = requestQueue.get(keys[i]);
                        Log.e("SOCKETCLASS","RESENDING "+r.key);
                        socket.emit(r.key,r.value);
                    }
                }
            });
    }

    static class Request{
        public String key;
        public Object value;
        private Response dataReady;

        public Object exec(Object obj){
            return dataReady.ready(obj);
        }

        public Request(String _key,Object _val,Response respn){
            this.key=_key;
            this.value=_val;
            this.dataReady=respn;
        }
    }
}