package com.ummo.prod.agent.adapter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ummo.prod.agent.R;
import com.ummo.prod.agent.ummoAPI.QueueJoinWrapper;

import java.util.List;

/**
 * Created by mosaic on 9/4/17.
 **/

public class QueueListAdapter extends RecyclerView.Adapter<QueueListAdapter.ViewHolder> {
    private List<QueueJoinWrapper> mDataset;
    String TAG = "QueueListAdpt.";

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView mTextView, position;
        public TextView cell;
        public LinearLayout mLy;
        ViewHolder(RelativeLayout v) {
            super(v);
            this.mTextView= v.findViewById(R.id.uname_tv);
            this.position = v.findViewById(R.id.position_text);
            this.cell = v.findViewById(R.id.booker_details);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public QueueListAdapter(List<QueueJoinWrapper> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public QueueListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        RelativeLayout ly = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.queueing_item, parent, false);
        // create a new view

        // set the view's size, margins, paddings and layout parameters
        // vh.mTextView.setText("Hello World");
        return new ViewHolder(ly);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset.get(position).getuName());
        holder.cell.setText(mDataset.get(position).getUserCell());
        //Log.e(TAG+" onBindVH", "Position->"+mDataset.get(position).getPosition());
        holder.position.setText("#"+mDataset.get(position).getPosition());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

