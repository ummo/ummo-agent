package com.ummo.prod.agent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.room.AppDatabase;
import com.ummo.prod.agent.ummoAPI.QueueJoinWrapper;

import java.util.List;
import java.util.Random;

public class SwipeDeckAdapter extends BaseAdapter {
    private List<QueueJoinWrapper> data;
    private Context context;
    private TextView name;
    private TextView alphaNums;
    private int[] mMaterialColors;
    private String[] names;
    private String[] alphas;
    private Random RANDOM = new Random();
    private MaterialLetterIcon mIcon;
    private AppDatabase appDatabase;

    public SwipeDeckAdapter(List<QueueJoinWrapper> data, Context context) {
        this.data = data;
        this.context = context;
    }

    /*public SwipeDeckAdapter(ArrayList<String> retrievedQueueData, ManageQueue context) {
        this.data = retrievedQueueData;
        this.context = context;
    }*/

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context =parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        // normally use a viewholder
        View v = inflater.inflate(R.layout.ccv, parent, false);
        name = v.findViewById(R.id.email);
        alphaNums = v.findViewById(R.id.alphaNumeric);
        mIcon = v.findViewById(R.id.icon);

        mMaterialColors = v.getContext().getResources().getIntArray(R.array.colors);
        names = v.getContext().getResources().getStringArray(R.array.string_array_name);
        alphas = v.getContext().getResources().getStringArray(R.array.alphanums);

        mIcon.setShapeColor(mMaterialColors[position]);
        mIcon.setInitials(true);
        mIcon.setInitialsNumber(2);
        mIcon.setLetterSize(18);
        String numCode = "";

        mIcon.setLetter(data.get(position).getuName());
        name.setText(data.get(position).getuName());
        alphaNums.setText(data.get(position).getNumCode());

        //((TextView) v.findViewById(R.id.textView2)).setText(data.get(position));
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String item = (String)getItem(position);
                //Log.i("MainActivity", item);
            }
        });
        return v;
    }
}