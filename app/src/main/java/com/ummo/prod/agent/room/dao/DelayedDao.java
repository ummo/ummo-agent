package com.ummo.prod.agent.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ummo.prod.agent.room.entities.Delayed;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface DelayedDao {
    //Create
    @Insert(onConflict = REPLACE)
    void insertDelayed(Delayed delayed);

    //Read
    @Query("select * from delayed")
    List<Delayed> loadDelayed();

    //Update
    @Update
    void updateDelayed(Delayed delayed);

    //Delete
    @Delete
    void deleteDelayed(Delayed delayed);
}
