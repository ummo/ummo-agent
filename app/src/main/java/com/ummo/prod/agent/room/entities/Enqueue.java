package com.ummo.prod.agent.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Enqueue {
    @PrimaryKey
    @NonNull
    public String enq_contact;

    public String enq_name;

    //TODO:Convert to Timestamp
    /*public String enq_time;

    public int enq_position;*/
}