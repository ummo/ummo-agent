package com.ummo.prod.agent.view;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.UmmoAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MonitorTabsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor_tabs);

        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);

        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setSubtitle("View Service Analytics");
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        /*
      The {@link ViewPager} that will host the section contents.
     */
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                setTitle(position==0?"Monitor Queues":"Monitor Bookings");
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Mixpanel Tracking of Agent Sign-Up
                try{
                    JSONObject props = new JSONObject();
                    props.put("print_report_fab","clicked");
                    mixpanelAPI.track("Monitor Service Activity",props);
                } catch (JSONException e) {
                    Log.e("MonitorTabsActivity", "Unable to add properties to JSONObject", e);
                }
                Snackbar.make(view, "'Print Analytics' Coming Soon...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_monitor_tabs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        int sectionNumber=1;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            Log.e("NEWINST",""+sectionNumber);

            return fragment;
        }
        public  View displayBookingsGraphs(LayoutInflater inflater,ViewGroup container){

            String mixpanelProjectToken = String.valueOf(R.string.mixpanelProjectTokenString);
            MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getContext(), mixpanelProjectToken);

            View rootView = inflater.inflate(R.layout.fragment_monitor_tabs, container, false);
            GraphView graphView = (GraphView) rootView.findViewById(R.id.graph);
            JSONArray bookinjsJA = UmmoAgent.getApp().bookingGraphData;
            LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
            StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graphView);
            String[] labels = new String[bookinjsJA.length()];
            if (bookinjsJA!=null){
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    graphView.removeAllSeries();
                    for(int i=0;i<bookinjsJA.length();i++){
                        try{
                            Date myDate = simpleDateFormat.parse(bookinjsJA.getJSONObject(i).getString("_id"));
                            int bkCount = bookinjsJA.getJSONObject(i).getInt("bkCount");
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(myDate);
                            labels[i]=""+cal.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.SHORT,Locale.getDefault());
                            Log.e("DATE",""+cal.get(Calendar.DAY_OF_MONTH)+" "+cal.getDisplayName(Calendar.MONTH,Calendar.SHORT,Locale.getDefault()));
                            series.appendData(new DataPoint(i,bkCount),false,bookinjsJA.length());
                            Log.e("DATAPOINT","X:"+i+" Y:"+bkCount);

                            //Mixpanel Tracking of Agent Sign-Up
                            try{
                                JSONObject props = new JSONObject();
                                props.put("booking_analytics_graph","viewed");
                                mixpanelAPI.track("Monitor Service",props);
                            } catch (JSONException e) {
                                Log.e("MonitorTabsActivity", "Unable to add properties to JSONObject", e);
                            }

                        }catch (ParseException pex){
                            Log.e(getClass().getCanonicalName(),pex.toString());
                        }
                    }
                    series.setDrawDataPoints(true);
                    series.setDrawAsPath(true);
                    graphView.setTitle("Bookings Made Per Day");

                    graphView.addSeries(series);
                    graphView.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
                    graphView.getGridLabelRenderer().setHorizontalAxisTitle("Date");
                    graphView.getGridLabelRenderer().setVerticalAxisTitle("Number of Bookings");
                    if(labels.length<1) return null;
                    staticLabelsFormatter.setHorizontalLabels(labels);
                }catch (JSONException jse){
                    Log.e("BookingsGraph",jse.toString());
                }
            }
            return rootView;
        }

        public View displayEmptyView(LayoutInflater inflater,ViewGroup container){
            View rootView = inflater.inflate(R.layout.fragment_monitor_empty, container, false);
            return rootView;
        }

        public View displayQueuesGraph(LayoutInflater inflater, ViewGroup container){
            String mixpanelProjectToken = String.valueOf(R.string.mixpanelProjectTokenString);
            MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getContext(), mixpanelProjectToken);

            View rootView = inflater.inflate(R.layout.fragment_monitor_queues, container, false);
            GraphView graphView = (GraphView) rootView.findViewById(R.id.graph);
            JSONArray bookinjsJA = UmmoAgent.getApp().bookingGraphData;
            LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
            StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graphView);
            String[] labels = new String[bookinjsJA.length()];
            if (bookinjsJA!=null){
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    simpleDateFormat.setTimeZone(TimeZone.getDefault());
                    graphView.removeAllSeries();
                    for(int i=0;i<bookinjsJA.length();i++){
                        try{
                            Date myDate = simpleDateFormat.parse(bookinjsJA.getJSONObject(i).getString("_id"));
                            int bkCount = bookinjsJA.getJSONObject(i).getInt("bkCount");
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(myDate);
                            labels[i]=""+cal.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.SHORT,Locale.getDefault());
                            Log.e("DATE",""+cal.get(Calendar.DAY_OF_MONTH)+" "+cal.getDisplayName(Calendar.MONTH,Calendar.SHORT,Locale.getDefault()));
                            series.appendData(new DataPoint(i,bkCount),false,bookinjsJA.length());
                            Log.e("DATAPOINT","X:"+i+" Y:"+bkCount);
                            //Mixpanel Tracking of Agent Sign-Up
                            try{
                                JSONObject props = new JSONObject();
                                props.put("queues_analytics_graph","viewed");
                                mixpanelAPI.track("Monitor Service",props);
                            } catch (JSONException e) {
                                Log.e("MonitorTabsActivity", "Unable to add properties to JSONObject", e);
                            }
                        }catch (ParseException pex){
                            Log.e(getClass().getCanonicalName(),pex.toString());
                        }
                    }
                    series.setDrawDataPoints(true);
                    series.setDrawAsPath(true);
                    graphView.setTitle("Queue Joins Per Day");

                    graphView.addSeries(series);
                    graphView.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
                    graphView.getGridLabelRenderer().setHorizontalAxisTitle("Date");
                    graphView.getGridLabelRenderer().setVerticalAxisTitle("Number of Bookings");
                    staticLabelsFormatter.setHorizontalLabels(labels);
                }catch (JSONException jse){
                    Log.e("BookingsGraph",jse.toString());
                }
            }
            return rootView;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getActivity().setTitle(getArguments().getInt(ARG_SECTION_NUMBER)==1?"Monitor Queues":"Monitor Bookings");
            Log.e("SECTION",""+getArguments().getInt(ARG_SECTION_NUMBER));

           // TextView textView = (TextView) rootView.findViewById(R.id.section_label);
           // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            if(UmmoAgent.getApp().bookingGraphData==null|| UmmoAgent.getApp().bookingGraphData.length()==0)return displayEmptyView(inflater,container);
            return getArguments().getInt(ARG_SECTION_NUMBER)==1?displayBookingsGraphs(inflater,container):displayQueuesGraph(inflater,container);
        }
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Log.e("GET ITEM"," "+position);
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            Log.e("PAGETITLE ",""+position);
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}
