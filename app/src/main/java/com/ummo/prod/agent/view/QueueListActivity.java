package com.ummo.prod.agent.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.adapter.QueueListAdapter;
import com.ummo.prod.agent.room.AppDatabase;
import com.ummo.prod.agent.room.entities.Delayed;
import com.ummo.prod.agent.room.entities.Queue;
import com.ummo.prod.agent.room.entities.Served;
import com.ummo.prod.agent.ummoAPI.QueueJoinWrapper;
import com.ummo.prod.agent.ummoAPI.QueueWrapper;
import com.ummo.prod.agent.viewModel.QueueListViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class QueueListActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private QueueListAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView.LayoutManager mLayoutManager;


    private static final String BUNDLE_EXTRAS = "BUNDLE EXTRAS";
    private static final String EXTRA_DETAIL = "EXTRA_DETAIL";
    private static final String EXTRA_ATTR = "EXTRA ATTR";
    private static final String TAG = "QueueList";
    private List<QueueJoinWrapper> joinsData = new ArrayList<>();
    private List<QueueJoinWrapper> roomQueueMembers = new ArrayList<>();
    private List<Delayed> delayedList = new ArrayList<>();
    private List<Served> servedList = new ArrayList<>();
    private AppDatabase appDatabase;
    SwipeRefreshLayout refreshLayout;

    private QueueListViewModel queueListViewModel;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText("Delayed Users");
                    joinsData.clear();
                    /*for(QueueJoinWrapper join:QueueWrapper.unsent){
                        joinsData.add(join);
                        Log.e("Loading Join",join.toString());
                    }*/
                    fetchDelayedUsersFromRoom();
                    joinsData.addAll(roomQueueMembers);
                    Log.e("ARRAY_LENGTH",""+joinsData.size());
                    mAdapter.notifyDataSetChanged();
                    return true;

                case R.id.navigation_dashboard: {
                    mTextMessage.setText(R.string.title_dashboard);
                    joinsData.clear();
                    for (QueueJoinWrapper join:QueueJoinWrapper.qmanQJoins){
                        joinsData.add(join);
                        Log.e("Loading Join",join.toString());
                    }
                    //joinsData=QueueJoinWrapper.qmanQJoins;
                    Log.e("ARRAY_LENGTH",""+QueueJoinWrapper.qmanQJoins);
                    mAdapter.notifyDataSetChanged();
                    return true;
                }
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    //roomQueueMembers.clear();
                    joinsData.clear();
                    joinsData.addAll(roomQueueMembers);
                    return true;
            }
            return false;
        }

    };

    private ItemTouchHelper.Callback createHelperCallback() {
        return new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
               // moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                //Mixpanel Tracking of Agent Booking Swipe
                final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
                final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), mixpanelProjectToken);
                try{
                    JSONObject props = new JSONObject();
                    props.put("bookingItem_vh","swiped");
                    mixpanelAPI.track("Manage Booking",props);
                } catch (JSONException e) {
                    Log.e("Console Activity", "Unable to add properties to JSONObject", e);
                }
               // deleteItem(viewHolder.getAdapterPosition());
                Log.e("POS SWIPED", String.valueOf(viewHolder.getAdapterPosition()));
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue_list);
        mRecyclerView = findViewById(R.id.queueing_rv);

        //Room DB instantiation
        appDatabase = AppDatabase.getInMemoryDatabase(getApplicationContext());
        fetchQueueMembersFromRoom();
        //Instantiating QueueListViewModel (ViewModel)
        //queueListViewModel = ViewModelProviders.of(this).get(QueueListViewModel.class);
        //Transferring ViewModel roomQueueMembers to local roomQueueMembers
        //_roomQueueMembers = queueListViewModel.getRoomQueueMembers();

        //Log.e(TAG+" onCreate", "RoomQueueMembers->"+roomQueueMembers.get(0).getUserCell());

        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

       // mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        //joinsData=QueueJoinWrapper.qmanQJoins;
        mAdapter = new QueueListAdapter(joinsData);
        mRecyclerView.setAdapter(mAdapter);
        Log.e("QUEUE-LIST",QueueJoinWrapper.qmanQJoins.size()+"");
    }

    private void fetchDelayedUsersFromRoom(){
        appDatabase = AppDatabase.getInMemoryDatabase(getApplicationContext());
        List<Delayed> _delayedUsers = appDatabase.delayedDao().loadDelayed();

        for (int i = 0; i< _delayedUsers.size(); i++){
            String _delayedUserContact = _delayedUsers.get(i).user_contact;
            String _delayedUserJoinTime = _delayedUsers.get(i).joinTime;
            String _delayedUserDelayTime = _delayedUsers.get(i).delayed_time;

            Log.e(TAG+" delayedUsers", "Contact->"+_delayedUserContact+"; JoinTime->"+_delayedUserJoinTime+"; DelayeTime->"+_delayedUserDelayTime);
        }
    }

    private void fetchQueueMembersFromRoom() {
        appDatabase = AppDatabase.getInMemoryDatabase(getApplicationContext());
        List<Queue> _roomQueueMembers = appDatabase.queueDao().loadQueueList();

        JSONObject _queue_member = new JSONObject();
        for (int j = 0; j < _roomQueueMembers.size(); j++) {
            Log.e(TAG + " fetching", "Retrieved Members (Room)->" + _roomQueueMembers.get(j).position);
            String _member_id = _roomQueueMembers.get(j).member_id;
            String _user_name = _roomQueueMembers.get(j).user_name;
            Log.e(TAG + " fetching", "Room User-Name->" + _user_name);
            String _num_code = _roomQueueMembers.get(j).num_code;
            int _position = _roomQueueMembers.get(j).position;
            _queue_member = new JSONObject();
            JSONObject user_object = new JSONObject();
            JSONObject full_name = new JSONObject();
            QueueJoinWrapper queue_member;
            try {
                _queue_member.put("userCell", _member_id);
                _queue_member.put("possition", _position);
                _queue_member.put("_id", _num_code);

                full_name.put("firstName", _user_name);
                //full_name.put("surName",_user_name);
                user_object.put("fullName", full_name);
                _queue_member.put("user", user_object);

                Log.e(TAG + " fetching", "Queue_Member->>>" + _queue_member);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG + " fetching", "Err... Error!!!", e);
            }
            queue_member = new QueueJoinWrapper(_queue_member);
            roomQueueMembers.add(queue_member);
            //roomQueueMembers.add(new QueueJoinWrapper(_queue_member));
            Log.e(TAG + " fetching", "RoomQueueMembers->" + roomQueueMembers.get(j).getUserCell()
                    + " Names->" + roomQueueMembers.get(j).getuName());

            Log.e(TAG + " fetching", "Final Storage of Member->" +
                    _roomQueueMembers.get(j).user_name + " Name->" +
                    _roomQueueMembers.get(j).member_id + "\n");
        }
        //return roomQueueMembers;

        if (_queue_member.has("user")) {
            try {
                JSONObject fullName = _queue_member.getJSONObject("fullName");
                String firstName = fullName.getString("firstName");

                Log.e(TAG + " Deep Check", "FirstName->>>" + firstName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            Log.e(TAG + " Deep Check", "Didn't Work!!!");
    }
}
