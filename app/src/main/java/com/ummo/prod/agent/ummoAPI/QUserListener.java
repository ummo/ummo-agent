package com.ummo.prod.agent.ummoAPI;

/**
 * Created by barnes on 3/6/16.
 */
public interface QUserListener
{
    void userRegistered(String string);
    void qJoined(String string);
    void qLeft(String string);
    void updated(String string, String position);
    void categoriesReady(String string);
    void allQsReady(String string);
    void qReady(String string);
    void gotJoinedQs(String string);

    //Errors
    void joinedQsError(String err);
    void userRegistrationError(String err);
    void qJoinedError(String err);
    void qLeftError(String err);
    void updateError(String err);
    void categoriesError(String err);
    void allQError(String err);
    void qError(String err);
}
