package com.ummo.prod.agent;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Switch;

import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.TextPosition;
import com.ummo.prod.agent.view.Console;


/**
 * Created by barnes on 11/14/16.
 */

public class MainActivity_fb extends Activity
{
    private static final int FRAMEWORK_REQUEST_CODE = 1;
    //private Switch advancedUISwitch;
    //private ButtonType confirmButton;
    //private ButtonType entryButton;
    private String initialStateParam;
    private int selectedThemeId = -1;
    private BroadcastReceiver switchLoginTypeReceiver;
    private TextPosition textPosition;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fb);

        if (AccountKit.getCurrentAccessToken() != null)
        {
            showHelloActivity();
        }
        onLogin(LoginType.EMAIL);
    }

    final MainActivity_fb thisActivity = this;

    @Override
    protected void onDestroy()
    {
        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(switchLoginTypeReceiver);

        super.onDestroy();
    }

    public void onLoginEmail(final View view) {
        onLogin(LoginType.EMAIL);
    }

    /*public void onLoginPhone(final View view) {
        onLogin(LoginType.PHONE);
    }*/

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FRAMEWORK_REQUEST_CODE) {
            final AccountKitLoginResult loginResult =
                    data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            final String toastMessage;
            if (loginResult.getError() != null) {
                //toastMessage = loginResult.getError().getErrorType().getMessage();
                showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                final AccessToken accessToken = loginResult.getAccessToken();
                final String authorizationCode = loginResult.getAuthorizationCode();
                final long tokenRefreshIntervalInSeconds =
                        loginResult.getTokenRefreshIntervalInSeconds();
                if (accessToken != null) {
                    toastMessage = "Success:" + accessToken.getAccountId()
                            + tokenRefreshIntervalInSeconds;
                    //showHelloActivity(loginResult.getFinalAuthorizationState());
                } else if (authorizationCode != null) {
                    toastMessage = String.format(
                            "Success:%s...",
                            authorizationCode.substring(0, 10));
                    showHelloActivity(authorizationCode, loginResult.getFinalAuthorizationState());
                } else {
                    toastMessage = "Unknown response type";
                }
            }

            /*Toast.makeText(
                    this,
                    toastMessage,
                    Toast.LENGTH_LONG)
                    .show();*/
        }
    }

    private AccountKitActivity.ResponseType getResponseType() {
        final Switch responseTypeSwitch = (Switch) findViewById(R.id.response_type_switch);
        if (responseTypeSwitch != null && responseTypeSwitch.isChecked()) {
            return AccountKitActivity.ResponseType.TOKEN;
        } else {
            return AccountKitActivity.ResponseType.CODE;
        }
    }

    private AccountKitConfiguration.AccountKitConfigurationBuilder createAccountKitConfiguration(final LoginType loginType)
    {
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder( loginType, getResponseType());
        /*final Switch titleTypeSwitch = (Switch) findViewById(R.id.title_type_switch);
        final Switch stateParamSwitch = (Switch) findViewById(R.id.state_param_switch);
        final Switch facebookNotificationsSwitch = (Switch) findViewById(R.id.facebook_notification_switch);
        final Switch useManualWhiteListBlacklist = (Switch) findViewById(R.id.whitelist_blacklist_switch);
        final Switch readPhoneStateSwitch = (Switch) findViewById(R.id.read_phone_state_switch);
        final Switch receiveSMS = (Switch) findViewById(R.id.receive_sms_switch);

        if (titleTypeSwitch != null && titleTypeSwitch.isChecked())
        {
            configurationBuilder.setTitleType(AccountKitActivity.TitleType.APP_NAME);
        }
        if (advancedUISwitch != null && advancedUISwitch.isChecked())
        {
            if (isReverbThemeSelected())
            {
                if (switchLoginTypeReceiver == null)
                {
                    switchLoginTypeReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(final Context context, final Intent intent) {
                            final String loginTypeString
                                    = intent.getStringExtra(ReverbUIManager.LOGIN_TYPE_EXTRA);
                            if (loginTypeString == null) {
                                return;
                            }
                            final LoginType loginType = LoginType.valueOf(loginTypeString);
                            if (loginType == null) {
                                return;
                            }
                            onLogin(loginType);
                        }
                    };
                    LocalBroadcastManager.getInstance(getApplicationContext())
                            .registerReceiver(
                                    switchLoginTypeReceiver,
                                    new IntentFilter(ReverbUIManager.SWITCH_LOGIN_TYPE_EVENT));
                }
                configurationBuilder.setAdvancedUIManager(new ReverbUIManager(
                        confirmButton,
                        entryButton,
                        loginType,
                        textPosition,
                        selectedThemeId));
            } else {
                configurationBuilder.setAdvancedUIManager(new AccountKitSampleAdvancedUIManager(
                        confirmButton,
                        entryButton,
                        textPosition));
            }
        }*/
        /*if (stateParamSwitch != null && stateParamSwitch.isChecked())
        {
            initialStateParam = UUID.randomUUID().toString();
            configurationBuilder.setInitialAuthState(initialStateParam);
        }
        if (facebookNotificationsSwitch != null && !facebookNotificationsSwitch.isChecked())
        {
            configurationBuilder.setFacebookNotificationsEnabled(false);
        }
        if (selectedThemeId > 0)
        {
            configurationBuilder.setTheme(selectedThemeId);
        }
        if (useManualWhiteListBlacklist != null && useManualWhiteListBlacklist.isChecked())
        {
            final String[] blackList = getResources().getStringArray(R.array.blacklistedSmsCountryCodes);
            final String[] whiteList = getResources().getStringArray(R.array.whitelistedSmsCountryCodes);
            configurationBuilder.setSMSBlacklist(blackList);
            configurationBuilder.setSMSWhitelist(whiteList);
        }

        if (readPhoneStateSwitch != null && !(readPhoneStateSwitch.isChecked()))
        {
            configurationBuilder.setReadPhoneStateEnabled(false);
        }
        if (receiveSMS != null && !receiveSMS.isChecked())
        {
            configurationBuilder.setReceiveSMS(false);
        }*/

        return configurationBuilder;
    }

    private boolean isReverbThemeSelected() {
        return selectedThemeId == R.style.AppLoginTheme_Reverb_A
                || selectedThemeId == R.style.AppLoginTheme_Reverb_B
                || selectedThemeId == R.style.AppLoginTheme_Reverb_C;
    }

    private void onLogin(final LoginType loginType) {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = createAccountKitConfiguration(loginType);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, FRAMEWORK_REQUEST_CODE);
        //Intent i = new Intent(this, Console.class);
        //startActivity(i);
    }

    private void showHelloActivity()
    {
        /*final Intent intent = new Intent(this, TokenActivity.class);
        intent.putExtra(TokenActivity.HELLO_TOKEN_ACTIVITY_INITIAL_STATE_EXTRA, initialStateParam);
        intent.putExtra(TokenActivity.HELLO_TOKEN_ACTIVITY_FINAL_STATE_EXTRA, finalState);
        startActivity(intent);*/
        //Intent i = new Intent(MainActivity_fb.this, Console.class);
        //startActivity(i);
        startActivity(new Intent(MainActivity_fb.this, Console.class));
        MainActivity_fb.this.finish();
    }

    private void showHelloActivity(final String code, final String finalState)
    {
        final Intent intent = new Intent(this, CodeActivity.class);
        intent.putExtra(CodeActivity.HELLO_CODE_ACTIVITY_CODE_EXTRA, code);
        intent.putExtra(CodeActivity.HELLO_CODE_ACTIVITY_INITIAL_STATE_EXTRA, initialStateParam);
        intent.putExtra(CodeActivity.HELLO_CODE_ACTIVITY_FINAL_STATE_EXTRA, finalState);
        startActivity(intent);
    }

    private void showErrorActivity(final AccountKitError error)
    {
        final Intent intent = new Intent(this, ErrorActivity.class);
        intent.putExtra(ErrorActivity.HELLO_TOKEN_ACTIVITY_ERROR_EXTRA, error);
        startActivity(intent);
    }
}