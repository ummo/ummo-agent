package com.ummo.prod.agent.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.events.UmmoEventListener;
import com.ummo.prod.agent.room.AppDatabase;
import com.ummo.prod.agent.room.entities.Agent;

import org.json.JSONException;
import org.json.JSONObject;


public class SignupActivity extends AppCompatActivity {

    Button signup;
    private AppDatabase appDatabase;
    private static final String TAG = "SignUpAct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Initializing AppDatabase
        appDatabase = AppDatabase.getInMemoryDatabase(getApplicationContext());

        setContentView(R.layout.accountkit_activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);

        TextView linkTV = findViewById(R.id.privacyLink);
        linkTV.setClickable(true);
        linkTV.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<div>By Signing Up, you agree to Ummo's <a href='http://privacy_policy.ummo.xyz/UmmoTermsofUse.pdf'>Terms of Use</a> and <a href='http://privacy_policy.ummo.xyz/'> Privacy Policy </a></div>";
        linkTV.setText(Html.fromHtml(text));

        if(UmmoAgent.getApp().isRegistered())startActivity(new Intent(SignupActivity.this,Console.class));

        signup = findViewById(R.id.btn_login);
        signup.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongViewCast")
            @Override
            public void onClick(View v) {

                final String id = UmmoAgent.getApp().getUmmoId();

                final AlertDialog.Builder _builder = new AlertDialog.Builder(SignupActivity.this);
                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                assert inflater != null;
                final View view = inflater.inflate(R.layout.layout_bookig_loading, null);
                _builder.setView(view);
                final AlertDialog dialog = _builder.create();
                dialog.show();

                Log.e("UmmoID",""+id);
                final String email =((AppCompatEditText)findViewById(R.id.email)).getText().toString();
                final String code = ((AppCompatEditText)findViewById(R.id.code)).getText().toString();

                //Mixpanel Tracking of Agent Sign-Up
                try{
                    JSONObject props = new JSONObject();
                    props.put("Ummo ID",""+id);
                    props.put("Email", ""+email);
                    props.put("Code", ""+code);
                    mixpanelAPI.track("SignUp",props);
                } catch (JSONException e) {
                    Log.e("SignUp Activity", "Unable to add properties to JSONObject", e);
                }

                Log.e("SIGN-UP",email+ " " + code);
                if((email.length()<4)||(code.length()<4)){
                    //Toast.makeText(SignupActivity.this,"All Fields are required",Toast.LENGTH_LONG).show();
                    ((AppCompatEditText)findViewById(R.id.email)).setError("Try a valid email!");
                    ((AppCompatEditText)findViewById(R.id.code)).setError("Your code should be four digits!");
                    return;
                }

                UmmoAgent.getApp().signup(email,code);
                UmmoAgent.getApp().registerEventListener("signup", new UmmoEventListener() {

                    @Override
                    public Object run(Object data) {
                        dialog.dismiss();
                        Intent i = new Intent(SignupActivity.this, Console.class);
                        startActivity(i);

                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                Agent agent = new Agent();
                                agent.agent_email = email;
                                agent.agent_code = code;
                                agent.agent_id = id;
                                Log.e(TAG+" onClick", "Creating agent->"+agent.agent_email);
                                appDatabase.agentDao().createAgent(agent);
                            }
                        });
                        finish();
                        return null;
                    }
                });
                UmmoAgent.getApp().registerEventListener("LOGIN-ERROR", new UmmoEventListener() {
                    @Override
                    public Object run(Object data) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SignupActivity.this,"The Email or Verification Code is incorrect... Please Try Again",Toast.LENGTH_LONG).show();
                            }
                        });

                        return null;
                    }
                });
            }
        });
    }
}
