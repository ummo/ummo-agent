package com.ummo.prod.agent.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ummo.prod.agent.room.entities.Queue;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface QueueDao {
    //Create
    @Insert(onConflict = REPLACE)
    void insertQueueMember(Queue queue);

    //Read
    @Query("select * from queue")
    List<Queue> loadQueueList();

    //Update
    @Update
    void updateQueueMember(Queue queue);

//    @Query("update queue set position = () ")
//    void delayQueueMember(int pos, String userCell);

    //Delete
    @Delete
    void dequeueQueueMember(Queue queue);

    @Query("delete from queue where member_id = :userCell")
    void dequeueQueueMemberByContact(String userCell);
}
