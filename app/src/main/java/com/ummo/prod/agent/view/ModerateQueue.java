package com.ummo.prod.agent.view;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.room.AppDatabase;
import com.ummo.prod.agent.room.entities.Enqueue;
import com.ummo.prod.agent.ummoAPI.QueueWrapper;

import org.json.JSONException;
import org.json.JSONObject;

public class ModerateQueue extends AppCompatActivity {

    private SharedPreferences sharedPreferences, prefs;
    private String TAG = "ModerateQueue";
    private Activity activity;
    private Context context;
    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Initializing AppDatabase
        appDatabase = AppDatabase.getInMemoryDatabase(getApplicationContext());

        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        setContentView(R.layout.activity_moderate_queue);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Moderate Queue");

        String sharedPrefFile = "com.ummo.prod.agent";
        sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        //prefs = PreferenceManager.getDefaultSharedPreferences(context);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        moderateQueue();
    }

    private void moderateQueue(){

        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);

        Button addToQueue = findViewById(R.id.add_to_Q_button);
        addToQueue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nameEt = findViewById(R.id.mod_input_name);
                EditText cellEt = findViewById(R.id.mod_input_cell);
                String name = nameEt.getText().toString();
                String cell = cellEt.getText().toString();

                //Mixpanel Tracking of Agent Sign-Up
                try{
                    JSONObject props = new JSONObject();
                    props.put("qmod_add_user","clicked");
                    mixpanelAPI.track("Moderate Queue",props);
                } catch (JSONException e) {
                    Log.e("Moderate Queue Activity", "Unable to add properties to JSONObject", e);
                }

                if(name.length()<3){
                    Snackbar.make(v,"Make sure you entered the name",Snackbar.LENGTH_LONG).show();
                    nameEt.setError("Please enter a valid name!");
                    return;
                }
                if(cell.length()<7){
                    Snackbar.make(v,"Phone number should not be shorter than 7 digits",Snackbar.LENGTH_LONG).show();
                    cellEt.setError("The contact seems incomplete");
                    return;
                }

                QueueWrapper.qModJoin(name,cell);

//                Enqueue enqueue = new Enqueue();
//                enqueue.enq_contact = cell;
//                enqueue.enq_name = name;

//                appDatabase.enqueueDao().insertEnqueue(enqueue);

//                Log.e(TAG+" Enqueue", "Enqueue Values->"+enqueue.enq_contact+"; @->"+enqueue);

                SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
                //SharedPreferences.Editor editor = prefs.edit();
                preferencesEditor.putString("Q_MOD_NAME", name);
                preferencesEditor.putString("Q_MOD_CONTACT", cell);
                //editor.putString("Q_MOD_NAME", name);
                //editor.putString("Q_MOD_CONTACT", cell);
                //editor.apply();

                //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                preferencesEditor.apply();

                //Snackbar.make(v, nameEt.getText()+" added to Queue!", Snackbar.LENGTH_SHORT).show();
                //Toast.makeText(ModerateQueue.this, nameEt.getText()+" SMS WAS SENT ", Toast.LENGTH_LONG).show();
                finish();
                //startActivity(new Intent(ModerateQueue.this, ManageQueue.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        Log.e("MENU ITEM SELECTED",""+item.getItemId());
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        String name = sharedPreferences.getString("Q_MOD_NAME", "Empty Name");
        String contact = sharedPreferences.getString("Q_MOD_CONTACT", "Empty Contact");

        Log.e(TAG + "onPause", "Name: " + name + " Contact: " + contact);
    }

    @Override
    protected void onStop() {
        super.onStop();

        String name = sharedPreferences.getString("Q_MOD_NAME", "Empty Name");
        String contact = sharedPreferences.getString("Q_MOD_CONTACT", "Empty Contact");

        Log.e(TAG + "onStop", "Name: " + name + " Contact: " + contact);
    }
}
