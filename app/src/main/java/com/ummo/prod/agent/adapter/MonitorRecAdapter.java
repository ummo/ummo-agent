package com.ummo.prod.agent.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ummo.prod.agent.R;

/**
 * Created by José on 2017/04/11.
 **/

public class MonitorRecAdapter extends RecyclerView.Adapter<MonitorRecAdapter.ViewHolder>{
    private String[] dataset;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;
        ViewHolder(TextView view) {
            super(view);
            textView = view;
        }
    }

    public MonitorRecAdapter(String[] data){
        dataset = data;
    }

    @Override
    public MonitorRecAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView view = (TextView) LayoutInflater.from(parent.getContext()).
                inflate(R.layout.analytics_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(dataset[position]);
    }

    @Override
    public int getItemCount() {
        return dataset.length;
    }
}
