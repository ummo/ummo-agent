package com.ummo.prod.agent.view;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.ummo.prod.agent.ummoAPI.BookingWrapper;
import com.ummo.prod.agent.ummoAPI.interfaces.DataReadyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.lang.Thread.sleep;

public class BookingModActivity extends AppCompatActivity implements MonthLoader.MonthChangeListener, WeekView.EventClickListener, WeekView.EventLongPressListener{
    private WeekView mWeekView;
    private Calendar start;

    List<WeekViewEvent> events = new ArrayList<>();
    // private List<WeekViewEvent> = new
    private int month=0;
    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        Log.e("EVENT",event.toString()+eventRect.toString());
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {

    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> monthEvents=new ArrayList<>();
        month=month==0?newMonth:month;
        mWeekView.invalidate();
        for(WeekViewEvent event:events){
            if(event.getStartTime()==null){
                Log.e("NULL",event.toString());
                return null;
            }
            if(event.getStartTime().get(Calendar.MONTH)==newMonth)monthEvents.add(event);
        }
        mWeekView.invalidate();
        return newMonth==month?events:new ArrayList<WeekViewEvent>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_mod);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            //getSupportActionBar().setSubtitle("Tap on the menu for settings");
        }

        setTitle("Click the start time");
        mWeekView = (WeekView) findViewById(R.id.weekView);
        mWeekView.setOnEventClickListener(this);
        mWeekView.setMonthChangeListener(this);
        mWeekView.setEventLongPressListener(this);
        Log.e("NOW",""+Calendar.getInstance().getTime().toString());
        final CardView cv = (CardView)findViewById(R.id.fab);
        cv.animate().translationY(-300);
        cv.setVisibility(View.VISIBLE);
        initAvailableBookings();
        mWeekView.notifyDatasetChanged();
        mWeekView.goToDate(Calendar.getInstance());
        mWeekView.goToHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

        mWeekView.setEmptyViewClickListener(new WeekView.EmptyViewClickListener() {
            @Override
            public void onEmptyViewClicked(Calendar time) {
                int minRounded=time.get(Calendar.MINUTE)%5<3?(time.get(Calendar.MINUTE)-time.get(Calendar.MINUTE)%5):(time.get(Calendar.MINUTE)+(5-time.get(Calendar.MINUTE)%5));
                time.set(Calendar.MINUTE,minRounded);
                cv.animate().setStartDelay(100).translationY(0).setDuration(1000);
                cv.setVisibility(View.VISIBLE);
                AppCompatTextView tv = (AppCompatTextView) findViewById(R.id.date_tv);
                Log.e("Time",""+time.getTimeZone().toString()+" "+time.getTime().toString());
                String _time = time.getTime().toString();
                tv.setText(_time.substring(0,10)+","+_time.substring(10,16));
                start=time;
                Calendar _end = Calendar.getInstance();
                _end.setTime(start.getTime());
                _end.add(Calendar.MINUTE,1);
                WeekViewEvent event = new WeekViewEvent(0,"",start,_end);
                initAvailableBookings();
                events.add(event);
                mWeekView.invalidate();
                mWeekView.notifyDatasetChanged();
            }
        });

        final LinearLayout ly = (LinearLayout)findViewById(R.id.next_ly);
        ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(BookingModActivity.this);
                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                final View view = inflater.inflate(R.layout.end_time, null);
                final EditText durationEt = (EditText)view.findViewById(R.id.durationEt);
                final EditText nameEt = (EditText)view.findViewById(R.id.nameEt);
                final EditText cellEt = (EditText) view.findViewById(R.id.cellEt);
                final EditText serviceEt  = (EditText) view.findViewById(R.id.serviceEt);
                builder.setView(view)
                        .setPositiveButton("save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                //Mixpanel Tracking of Agent Save New Booking
                                try{
                                    JSONObject props = new JSONObject();
                                    props.put("booking_save","clicked");
                                    mixpanelAPI.track("Booking Created",props);
                                } catch (JSONException e) {
                                    Log.e("Manage Queue Activity", "Unable to add properties to JSONObject", e);
                                }

                                String service = serviceEt.getText().toString();
                                Calendar end = Calendar.getInstance();
                                Date _start = start.getTime();
                                end.setTime(_start);
                                String duration = durationEt.getText().toString();
                                String cell = cellEt.getText().toString();
                                String name = nameEt.getText().toString();


                                if(service.isEmpty()||service.length()<3||name.length()<3||name.isEmpty()||cell.length()<8||duration.isEmpty()){
                                    Snackbar snackbar = Snackbar.make(cv,"PLEASE CHECK ALL FIELDS!",Snackbar.LENGTH_INDEFINITE);
                                    View sbView = snackbar.getView();
                                    sbView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.red));
                                    //TextView snackbarText = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                    //snackbarText.setTextColor(getResources().getColor(R.color.red));
                                    snackbar.setActionTextColor(getResources().getColor(R.color.black));
                                    snackbar.setAction(R.string.retry, new retry());
                                    snackbar.show();
                                    return;
                                }

                                int durationVal = Integer.valueOf(duration);
                                end.add(Calendar.MINUTE,durationVal);
                                WeekViewEvent event = new WeekViewEvent(1234,name,"",start,end);

                                events.add(event);
                                mWeekView.notifyDatasetChanged();
                                BookingWrapper.createBooking(name,cell,start,end,service);
                                cv.animate().setStartDelay(100).translationY(-300);
                                final AlertDialog.Builder _builder = new AlertDialog.Builder(BookingModActivity.this);
                                LayoutInflater inflater = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                                final View view = inflater.inflate(R.layout.layout_bookig_loading, null);
                                _builder.setView(view);
                                _builder.create().show();

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            sleep(3000);

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(BookingModActivity.this,"WAITED 3secs",Toast.LENGTH_SHORT).show();
                                                    if(BookingWrapper.temp.get_id().length()>5){
                                                        finish();
                                                    }else {
                                                        BookingWrapper.saveTempBooking();
                                                        finish();
                                                    }
                                                }
                                            });
                                        }catch (InterruptedException iex){
                                            Log.e("BookingModActivity",iex.toString());
                                        }
                                    }
                                }).start();

                                BookingWrapper.setOnBookingCreated(new DataReadyCallBack() {
                                    @Override
                                    public Object exec(Object obj) {
                                        finish();
                                        return null;
                                    }
                                });
                                //finish();
                            }
                        })
                        .create().show();
            }
        });
    }



    public void initAvailableBookings(){
        events.clear();
        BookingWrapper.setOnLoadedBookings(new DataReadyCallBack() {
            @Override
            public Object exec(Object obj) {
                for (int i = 0; i < BookingWrapper.getBookings().size(); i++){
                    BookingWrapper bk = BookingWrapper.getBookings().get(i);
                    events.add(new WeekViewEvent(i,bk.getService(),bk.getStart(),bk.getEnd()));
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mWeekView.notifyDatasetChanged();
                    }
                });
                return null;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        Log.e("MENU ITEM SELECTED",""+item.getItemId());
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

class retry implements View.OnClickListener{
    @Override
    public void onClick(View v) {

    }
}
