package com.ummo.prod.agent.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.TextPosition;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.metrics.AddTrace;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.ummoAPI.QMasterListener;
import com.ummo.prod.agent.ummoAPI.QueueJoinWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.ummo.prod.agent.ummoAPI.QueueJoinWrapper.setData;


/**
 * Created by barnes on 8/6/15.
 **/
public class SplashScreen extends Activity implements QMasterListener
{
    //Overloads for QmasterListener
    private static final int FRAMEWORK_REQUEST_CODE = 1;
    //private Switch advancedUISwitch;
    //private ButtonType confirmButton;
    //private ButtonType entryButton;
    private String initialStateParam;
    private int selectedThemeId = -1;
    private BroadcastReceiver switchLoginTypeReceiver;
    private TextPosition textPosition;

    final String firebaseSenderID = "438150256556";
    private SharedPreferences sharedPreferences;
    private String sharedPrefFile = "com.ummo.prod.agent";
    public static final List<QueueJoinWrapper> qmanQJoins = new ArrayList<>();
    final ArrayList<QueueJoinWrapper> queueData = new ArrayList<>();
    private static final String TAG = "SplashScr";
    private static Socket socket= UmmoAgent.getApp().getSocket();

    @Override
    protected void onDestroy()
    {
        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(switchLoginTypeReceiver);

        super.onDestroy();
    }

    public void onLoginEmail(final View view) {
        onLogin(LoginType.EMAIL);
    }

    /*public void onLoginPhone(final View view) {
        onLogin(LoginType.PHONE);
    }*/

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FRAMEWORK_REQUEST_CODE) {
            final AccountKitLoginResult loginResult =
                    data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            final String toastMessage;
            if (loginResult.getError() != null) {
                //toastMessage = loginResult.getError().getErrorType().getMessage();
                //showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                final AccessToken accessToken = loginResult.getAccessToken();
                final String authorizationCode = loginResult.getAuthorizationCode();
                final long tokenRefreshIntervalInSeconds =
                        loginResult.getTokenRefreshIntervalInSeconds();
                if (accessToken != null) {
                    toastMessage = "Success:" + accessToken.getAccountId()
                            + tokenRefreshIntervalInSeconds;
                    //showHelloActivity(loginResult.getFinalAuthorizationState());
                } else if (authorizationCode != null) {
                    toastMessage = String.format(
                            "Success:%s...",
                            authorizationCode.substring(0, 10));
                    showHelloActivity(authorizationCode, loginResult.getFinalAuthorizationState());
                } else {
                    toastMessage = "Unknown response type";
                }
            }
            /*Toast.makeText(
                    this,
                    toastMessage,
                    Toast.LENGTH_LONG)
                    .show();*/
        }
    }

    private AccountKitActivity.ResponseType getResponseType() {
        final Switch responseTypeSwitch = findViewById(R.id.response_type_switch);
        if (responseTypeSwitch != null && responseTypeSwitch.isChecked()) {
            return AccountKitActivity.ResponseType.TOKEN;
        } else {
            return AccountKitActivity.ResponseType.CODE;
        }
    }

    private AccountKitConfiguration.AccountKitConfigurationBuilder createAccountKitConfiguration(final LoginType loginType)
    {
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder( loginType, getResponseType());
        return configurationBuilder;
    }

    private boolean isReverbThemeSelected() {
        return selectedThemeId == R.style.AppLoginTheme_Reverb_A
                || selectedThemeId == R.style.AppLoginTheme_Reverb_B
                || selectedThemeId == R.style.AppLoginTheme_Reverb_C;
    }

    private void onLogin(final LoginType loginType) {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = createAccountKitConfiguration(loginType);
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,configurationBuilder.build());
        startActivityForResult(intent, FRAMEWORK_REQUEST_CODE);
    }

    private void showHelloActivity()
    {
        final Intent i = new Intent(SplashScreen.this, Console.class);
        startActivity(i);
        SplashScreen.this.finish();
    }

    private void showHelloActivity(final String code, final String finalState) {
        final Intent i = new Intent(SplashScreen.this, Console.class);
        startActivity(i);
        SplashScreen.this.finish();
    }

    @Override
    public void qCreated(String string) {
    }

    @Override
    public void registrationError(String string) {
    }

    @Override
    public void createQError(String string) {
    }

    @Override
    public void registered(String string) {
    }

    @Override
    public void qDestroyed(String string) {
    }

    @Override
    public void userMoved(String string) {
    }

    @Override
    public void userDQd(String string) {
    }

    @Override
    public void feedBackReceived(String string) {
    }

    @Override
    public void myQReceived(String string) {

    }

    @Override
    public void updatesReceived(String string) {

    }

    @Override
    public void onUpdatesError(String string) {

    }

    @Override
    public void onFeedBackError(String string) {

    }

    @Override
    public void onUserMoveError(String string) {

    }

    @Override
    public void onUserDQError(String string) {

    }

    @Override
    public void onQDestroyError(String sting) {

    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    //End Overloads
    long Delay = 10000;
    @AddTrace(name = "onSplashTrace")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ummo_splash);

        boolean fabricInitialized = Fabric.isInitialized();
        if (fabricInitialized)
            Crashlytics.log("Failed to initialise!");

        Context context = this.getApplicationContext();
        String sentryDsn = getResources().getString(R.string.sentryDsn);
        //Fetching a new FirebaseInstanceID in the event the app needs to reload one, is uninstalled or app data is cleared
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("FCM TOKEN - ", "Refreshed token " + refreshedToken);
        //Instantiating MixpanelAPI and getting the instance needed
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        String agent_id= UmmoAgent.getApp().isRegistered()? UmmoAgent.getApp().getUmmoId():"NOT REGISTERED";
        String agent_name= UmmoAgent.getApp().isRegistered()? UmmoAgent.getApp().getUname():"NOT REGISTERED";
        String agent_email= UmmoAgent.getApp().isRegistered()? UmmoAgent.getApp().getEmail():"NOT REGISTERED";

        //Sentry
        Sentry.init(sentryDsn, new AndroidSentryClientFactory(context));
        Sentry.getContext().recordBreadcrumb(new BreadcrumbBuilder().setMessage("Agent launched app - splashScreen").build());
        Sentry.getContext().setUser(new UserBuilder().setEmail(agent_email).build());
        Sentry.getContext().setUser(new UserBuilder().setUsername(agent_name).build());
        Sentry.getContext().setUser(new UserBuilder().setId(agent_id).build());

        //Mixpanel People identifier (with an agent_id) and set analytical info for Mixpanel Insights
        mixpanel.identify(agent_id);
        mixpanel.getPeople().identify(agent_id);
        mixpanel.getPeople().set("User Name", agent_name);
        mixpanel.getPeople().set("User Email", agent_email);
        MixpanelAPI.People people = mixpanel.getPeople();
        people.identify(agent_name);
        people.initPushHandling(firebaseSenderID);

        // JSONArray jsonArray = (JSONArray) BookingData.getBookingData();
        //Log.e("Booking JSONArray", jsonArray.toString());

        try {
            JSONObject props = new JSONObject();
            props.put("State", "Launched");
            mixpanel.track("Splash Screen", props);
            Log.e("Mixpanel" , "Initiated");
        } catch (JSONException e) {
            Log.e("MYAPP", "Unable to add properties to JSONObject", e);
            //Sending JSONException via Sentry
            Sentry.capture(e);
        }

        new Handler().postDelayed(new Thread() {
            @Override
            public void run()
            {
                if(UmmoAgent.getApp().isRegistered())
                {
                    /*Intent login_register = new Intent(SplashScreen.this, AnimateToolbar.class);
                    SplashScreen.this.startActivity(login_register);
                    finish();

                    overridePendingTransition(R.layout.fadein, R.layout.fadeout);*/
                    //Log.e("Registered",UmmoAgent.getApp().getUname());
                    showHelloActivity();

                    //startActivity(new Intent(SplashScreen.this,MonitorTabsActivity.class));
                    SplashScreen.this.finish();
                }
                else
                {
                    startActivity(new Intent(SplashScreen.this,SignupActivity.class));
                    finish();
                }
            }
        }, Delay);

        /**
         * Saving loaded queue joins into sharedPreferences "saved_queue_joins"
         **/
        QueueJoinWrapper.loadData();

        socket.emit("agent_joins",UmmoAgent.getApp().getUmmoId());
        socket.off("agent_joins");
        socket.on("agent_joins", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray array=(JSONArray)args[0];
                setData(array);
                Log.e(TAG+" Loaded Queue",array.toString());
                SharedPreferences sharedPreferences =
                        getSharedPreferences(sharedPrefFile, Activity.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("SAVED_QUEUE_JOINS", array.toString());
                editor.apply();
            }
        });
    }

    public final boolean isInternetOn() {
        //get Connectivity Manager object to check connection
        ConnectivityManager connec = (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        // Check for network connections
        assert connec != null;
        if (    connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED  ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED )
        {
            //if connected with internet
            //Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;
        }
        else if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  )
        {
            Toast.makeText(this, "Please turn on your data or connect to a WiFi hotspot", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}