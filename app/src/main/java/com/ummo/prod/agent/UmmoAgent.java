package com.ummo.prod.agent;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.onesignal.OneSignal;
import com.ummo.prod.agent.events.UmmoEventListener;
import com.ummo.prod.agent.model.UmmoAgentWrapper;
import com.ummo.prod.agent.room.AppDatabase;
import com.ummo.prod.agent.view.AnimateToolbar;
import com.ummo.prod.agent.view.DiscoverActivity;
import com.ummo.prod.agent.view.Console;
import com.ummo.prod.agent.ummoAPI.QueueJoinWrapper;
import com.ummo.prod.agent.ummoAPI.QueueWrapper;
import com.ummo.prod.agent.view.ManageQueue;

import com.ummo.prod.agent.ummoAPI.SocketClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.net.URISyntaxException;
import java.util.HashMap;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by mosaic on 7/24/16.
 **/
public class UmmoAgent extends Application {
    private Socket mSocket;
    private static UmmoAgent app;
    private UmmoAgentWrapper wrapper;
    private String TAG = "UmmoAgent";
    JSONObject qmasterData;
    JSONArray qErs;
    private boolean isConnected = false;
    public JSONArray bookingGraphData=null;
    private HashMap<String,UmmoEventListener> ummoEventsHashMap = new HashMap<>();
    private AppDatabase appDatabase;

    public void registerEventListener(String event,UmmoEventListener listener){
        ummoEventsHashMap.put(event,listener);
    }

    public JSONObject getQmasterData() {
        return qmasterData;
    }

    public JSONArray getqErs() {
        return qErs;
    }

    public boolean isConnected(){
        return isConnected;
    }

    public void fireUmmoEvent(String event, Object data){
        Object object = ummoEventsHashMap.containsKey(event)?ummoEventsHashMap.get(event).run(data):null;
    }

    public void getBookingGraphData(){
        Log.e("BIG-DATA",getUmmoId());
        mSocket.emit("bookings_by_done",getUmmoId());
        mSocket.on("bookings_by_done", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bookingGraphData = (JSONArray)args[0];
                fireUmmoEvent("bookings_by_done",bookingGraphData);
                Log.e("P-ROCK",bookingGraphData.toString());
            }
        });
    }

    public void init(){
        app=this;
        wrapper =new UmmoAgentWrapper(this);
        //AccountKit.initialize(getApplicationContext());
    }

    public static UmmoAgent getApp(){
        return app;
    }
    public void logout(){
        appDatabase.agentDao().deleteAgent(getEmail(), getUmmoId());
        Log.e(TAG+" onLogOut", "Deleting Agent->"+ getEmail());
        wrapper.setRegistered(false);
        wrapper.save();
    }
    public boolean isRegistered(){
        return wrapper.isRegistered();
    }

    public String getUname(){
        return wrapper.getUname();
    }

    public String getCell(){
        return wrapper.getCell();
    }


    public String getEmail(){
        return wrapper.getEmail();
    }

    public void setUname(String _uname){
        wrapper.setUname(_uname);
        wrapper.save();
    }

    public void setCell(String cell){
        wrapper.setCell(cell);
        wrapper.save();
    }

    public void setUmmoId(String id){
        Log.e("Saving ID",id);
        wrapper.setUmmoId(id);
        wrapper.save();
    }

    public void setRegistered(){
        wrapper.setRegistered(true);
        wrapper.save();
    }

    public void setEmail(String email){
        wrapper.setEmail(email);
        wrapper.save();
    }

    public void register(String uname,String cell,String mail){
        /* Exaple qmaster Object JSON
        { __v: 0,
             uname: 'mosaic',
             cell: '76346637',
             email: 'mosaic1@ummo.xyz',
             _id: 57954a905e336d4b3c3db2f9 }
         */
        mSocket.on("qmaster-registered", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    if(args[0]==null){
                        return;
                    }
                    JSONObject me = (JSONObject)args[0];
                    setUname(me.getJSONObject("fullName").getString("firstName"));
                    setCell(me.getString("cell"));
                    setEmail(me.getString("email"));
                    setUmmoId(me.getString("_id"));
                    setRegistered();
                }
                catch (JSONException jse){
                    Log.e("REG",jse.toString());
                }

                fireUmmoEvent("qmaster-registered", args[0]);
                Log.e("REGISTERED",args[0].toString());
            }
        });

        JSONObject object = new JSONObject();
        try {
            object.accumulate("uname",uname);
            object.accumulate("cell",cell);
            object.accumulate("email",mail);
            mSocket.emit("qmaster-register",object);
        }
        catch (JSONException jse){
            Log.e("REGISTRATION",jse.toString());
        }
    }

    public String getUmmoId(){
        return  wrapper.getUmmoId();
    }
    public void getQMaster(){
        mSocket.on("qmaster", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
      /*          try {
                    qmasterData=(JSONObject) args[0];
                    Log.e("UNAME",qmasterData.getString("uname"));
                    Log.e("UNAME",qmasterData.toString());
                    if(qmasterData.has("managedQ")){
                        if(qmasterData.getJSONObject("managedQ")!=null){
                            populateQInfo();
                        }
                    }

                }
                catch (JSONException jse){
                    Log.e("QMASTER",jse.toString());
                }*/
            }
        });
        mSocket.emit("get-qmaster",getUmmoId());
    }

    public void populateQInfo(){
        Log.e("ON","POPULATE");
        getQErs();
        fireUmmoEvent("queue","DATA");
    }

    public void getQErs(){
     /*   try {
            String vq =qmasterData.getJSONObject("managedQ").getString("_id");
            mSocket.emit("get-quers",vq);
            Log.e("LOG","GETTING QERS");
        }
        catch (JSONException jse){
            Log.e("POPULATEQ",jse.toString());
        }*/
    }

    public void getServiceByEmail(String email){
        mSocket.on("service-emailed", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                fireUmmoEvent("service-emailed",args[0]);
            }
        });
        mSocket.emit("service-email",email);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void getBookingsFromServer(){

        if(!isRegistered())
            return;
       /* mSocket.emit("agent-bookings",getUmmoId());
        mSocket.on("agent-bookings", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bookingsArray = (JSONArray)args[0];
                assert bookingsArray!= null;
                //bWrapper.setBookeeData(bookingsArray);
                Log.e("UMMOADMIN",bookingsArray.toString());
                fireUmmoEvent("agent-bookings-ready", bookingsArray);
            }
        });*/
    }

    public void dq(){
        try {
            String vq = qmasterData.getJSONObject("managedQ").getString("_id");
            mSocket.emit("dq",vq);
            //getQErs();
        }
        catch (JSONException jse){
            Log.e("DQ",jse.toString());
        }
    }

    public void bookingDone(String _bid){
        mSocket.on("set-booking-done", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("UmmoAgent Booking Done",args[0].toString());
            }
        });
        mSocket.emit("set-booking-done",_bid);
    }

    public void signup(String email, String verCode){
        JSONObject object = new JSONObject();
        try{

            object.accumulate("email",email);
            object.accumulate("code",verCode);
            //mSocket.emit("signup",object);

            SocketClass.emit("signup", object, new SocketClass.Response() {
                @Override
                public Object ready(Object val) {
                    Log.e(TAG+" SIGN-UP",val.toString());
                    if (val.toString().length()<10){
                        fireUmmoEvent("LOGINERR",val);
                        return null;
                    }
                    try {
                        JSONObject me = (JSONObject)val;
                        setUname(me.getJSONObject("fullName").getString("firstName"));
                        setCell(me.getString("cell"));
                        setEmail(me.getString("email"));
                        setUmmoId(me.getString("_id"));
                        setRegistered();


                        String url = getString(R.string.SERVER_URL);

                        IO.Options opts = new IO.Options();
                        opts.query="uid="+getUmmoId();

                        Log.e(TAG+" OPTS QUERY",opts.query);

                        Log.e(TAG+" APPLICATION","App Created on ->"+url);
                        mSocket.close();
                        try {
                            mSocket = IO.socket(url, opts);
                            mSocket.connect();
                            SocketClass.init();
                        }catch (URISyntaxException ure){
                            Log.e(TAG+" URL ERROR",ure.toString());
                        }
                        mSocket.emit("agent",getUmmoId());
                        com.ummo.prod.agent.ummoAPI.BookingWrapper.loadBookings();
                        QueueJoinWrapper.loadData();
                        mSocket.on("agent_queue_joins", new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                Log.e(TAG+" GOT AGENT Joins",args[0].toString());
                            }
                        });
                        mSocket.on("qmaster_queueings", new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                Log.e(TAG+" QMASTER QUEUEINGS",args[0].toString());
                            }
                        });
                        mSocket.on("agent", new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                try {
                                    JSONObject object=(JSONObject) args[0];
                                    QueueWrapper.loadQueue(object.getJSONObject("managedQ").getString("_id"));
                                }catch (JSONException jse){
                                    Log.e("EGETMAAGEDQ",jse.toString());
                                }
                                Log.e(TAG+" Started ","AGENT"+args[0].toString());
                            }
                        });

                    }
                    catch (JSONException jse){
                        Log.e("REG",jse.toString());
                    }
                    Log.e(TAG+" UMMO-ADMIN","REGISTERED");
                    fireUmmoEvent("signup",null);
                    getQMaster();
                    return null;
                }
            });

        }

        catch (JSONException jse){
            Log.e(TAG+" JSON ERR",jse.toString());
        }
    }

    public Socket getSocket(){
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
        //AccountKit.initialize(getApplicationContext());
        String url = getString(R.string.SERVER_URL);

        appDatabase = AppDatabase.getInMemoryDatabase(getApplicationContext());
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        IO.Options opts = new IO.Options();
        opts.query="uid="+getUmmoId();

        Log.e("OPTS QUERY",opts.query);

        Log.e("APPLICATION","APP Created ON"+url);
        try{
            mSocket = IO.socket(url,opts);
            mSocket.connect();
            SocketClass.init();
            if(isRegistered()){
                getQMaster();
                getBookingsFromServer();
                getBookingGraphData();

            }
            mSocket.on("connect", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("Connection","UP");
                    isConnected =true;
                    fireUmmoEvent("onn","Onn");
                    SocketClass.init();

                    if(isRegistered()){
                        mSocket.emit("agent",getUmmoId());
                        com.ummo.prod.agent.ummoAPI.BookingWrapper.loadBookings();
                        QueueJoinWrapper.loadData();
                        QueueWrapper.loadData();
                        mSocket.on("agent_queue_joins", new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                Log.e(TAG+" GOT AGENT Joins",args[0].toString());
                            }
                        });
                        mSocket.on("qmaster_queueings", new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                Log.e(TAG+" QMASTER QUEUEINGS",args[0].toString());
                            }
                        });
                        mSocket.on("agent", new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                try {
                                    JSONObject object=(JSONObject) args[0];
                                    QueueWrapper.loadQueue(object.getJSONObject("managedQ").getString("_id"));
                                }catch (JSONException jse){
                                    Log.e("EGETMAAGEDQ",jse.toString());
                                }
                                Log.e(TAG+" Started ","AGENT ->"+args[0].toString());
                            }
                        });
                    }
                }
            });
            mSocket.on("disconnect", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("SOCKET","Disconnected");
                    isConnected =false;
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                synchronized (this){
                                    wait(10000);
                                    if(!isConnected){
                                        fireUmmoEvent("disconnected","disconnected");
                                        generalNotification("You are offline!","Please check your connection");
                                    }
                                }
                            }
                            catch (InterruptedException ie){
                                Log.e(TAG+" InterruptedException",ie.toString());
                            }
                        }
                    });

                    thread.start();
                }
            });

            mSocket.on("create-booking", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject booking = (JSONObject)args[0];
                    Log.e("BOOKING MADE",args[0].toString());
                    try{
                        getBookingsFromServer();
                        generalNotification("You have just been Booked","For "+booking.getString("service"));
                    }catch (JSONException jse){
                        Log.e("BOOKING",booking.toString());
                    }

                }
            });

            mSocket.on("message", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG+" Message",args[0].toString());
                }
            });

            mSocket.on("first-join", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    notifyFirstJoin();
                }
            });

            mSocket.on("qers", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    qErs = (JSONArray)args[0];
                    fireUmmoEvent("qers","qers");
                    Log.e(TAG+" QUERS",qErs.toString());
                }
            });

        }
        catch (URISyntaxException use){
            Log.e(TAG+" SOCKET ",use.toString());
        }
    }

    public void manageQueue(String queue){
        Log.e(TAG+" Managing ",queue);
        try {
            JSONObject object = new JSONObject();
            object.accumulate("queue",queue);
            object.accumulate("qmaster",getUmmoId());
            mSocket.emit("manage-q",object);
            getQMaster();
        }

        catch (JSONException jse){
            Log.e(TAG+" MANAGE-Q",jse.toString());
        }
    }

    public void swipeLeft(){
        mSocket.emit("swipe_back",QueueWrapper.managedQueue.get_id());
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    private void notifyFirstJoin() {
        Intent resultIntent = new Intent(this, ManageQueue.class);
        TaskStackBuilder stackBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder = TaskStackBuilder.create(this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            assert stackBuilder != null;
            stackBuilder.addParentStack(ManageQueue.class);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder.addNextIntent(resultIntent);
        }
        PendingIntent resultPendingIntent =
                null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
        }

        long[] parten = {500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122};
        /*********************************************************************************************************************/
        /****************Begining of Added Changes****************************************************************************/
        /*********************************************************************************************************************/

        long[] partens = new long[10];
        for (int i=0;i<100;i++){
//            System.arraycopy(parten,0,partens,parten.length*i,parten.length);
        }

        /**********************************************************************************************************************/
        /***************************End of Changes*****************************************************************************/
        /**********************************************************************************************************************/

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher).setContentTitle("First Client in your Queue")
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(partens)  /**********From pattern I changed to patterns*/
                .setContentText("Tap To View");
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(100, mBuilder.build());
    }

    private void generalNotification(String title, String body) {
        //Context context = getBaseContext();
       // if(joinedQs.size()<1) return;
        Intent resultIntent = new Intent(this, Console.class);

        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        TaskStackBuilder stackBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                ) {
            stackBuilder = TaskStackBuilder.create(this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            assert stackBuilder != null;
            stackBuilder.addParentStack(Console.class);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder.addNextIntent(resultIntent);
        }
        PendingIntent resultPendingIntent =
                null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
        }

        long[] parten = {500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122,500,122,300,122};
        /*********************************************************************************************************************/
        /****************Beginning of Added Changes****************************************************************************/
        /*********************************************************************************************************************/


        /**********************************************************************************************************************/
        /***************************End of Changes*****************************************************************************/
        /**********************************************************************************************************************/

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher).setContentTitle(title)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                // .setVibrate(partens)  /**********From pattern I changed to patterns*/
                .setContentText(getUname()+" "+body);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(101, mBuilder.build());
    }

    public void bookingCloseNotification(String bk_id,String service,String period,int hash){
        Intent resultIntent = new Intent(this, DiscoverActivity.class);
        resultIntent.putExtra("BOOKING_DONE_ID",bk_id);
        TaskStackBuilder stackBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                ) {
            stackBuilder = TaskStackBuilder.create(this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            assert stackBuilder != null;
            stackBuilder.addParentStack(AnimateToolbar.class);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            stackBuilder.addNextIntent(resultIntent);
        }
        PendingIntent resultPendingIntent =
                null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            resultPendingIntent = stackBuilder.getPendingIntent(
                    hash+1,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher).setContentTitle("Booking")
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(getUname()+" "+service+" is up in "+period);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(hash, mBuilder.build());
    }
}