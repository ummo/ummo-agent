package com.ummo.prod.agent.ummoAPI.interfaces;

/**
 * Created by mosaic on 5/11/17.
 */

public interface DataReadyCallBack {
    public Object exec(Object obj);
}
