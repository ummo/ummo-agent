package com.ummo.prod.agent.viewModel;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.util.Log;

import com.ummo.prod.agent.room.AppDatabase;
import com.ummo.prod.agent.room.entities.Queue;
import com.ummo.prod.agent.ummoAPI.QueueJoinWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class QueueListViewModel extends ViewModel{

    private static final String TAG = "QueueListVM";
    private AppDatabase appDatabase;
    private final ArrayList<QueueJoinWrapper> roomQueueMembers = new ArrayList<>();
    private Context context;

    public QueueListViewModel(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    public void fetchQueueMembersFromRoom() {
        appDatabase = AppDatabase.getInMemoryDatabase(context);
        List<Queue> _roomQueueMembers = appDatabase.queueDao().loadQueueList();

        JSONObject _queue_member = new JSONObject();
        for (int j = 0; j < _roomQueueMembers.size(); j++) {
            Log.e(TAG + " fetching", "Retrieved Members (Room)->" + _roomQueueMembers.get(j).position);
            String _member_id = _roomQueueMembers.get(j).member_id;
            String _user_name = _roomQueueMembers.get(j).user_name;
            Log.e(TAG + " fetching", "Room User-Name->" + _user_name);
            String _num_code = _roomQueueMembers.get(j).num_code;
            int _position = _roomQueueMembers.get(j).position;
            _queue_member = new JSONObject();
            JSONObject user_object = new JSONObject();
            JSONObject full_name = new JSONObject();
            QueueJoinWrapper queue_member;
            try {
                _queue_member.put("userCell", _member_id);
                _queue_member.put("possition", _position);
                _queue_member.put("_id", _num_code);

                full_name.put("firstName", _user_name);
                //full_name.put("surName",_user_name);
                user_object.put("fullName", full_name);
                _queue_member.put("user", user_object);

                Log.e(TAG + " fetching", "Queue_Member->>>" + _queue_member);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG + " fetching", "Err... Error!!!", e);
            }
            queue_member = new QueueJoinWrapper(_queue_member);
            roomQueueMembers.add(queue_member);
            //roomQueueMembers.add(new QueueJoinWrapper(_queue_member));
            Log.e(TAG + " fetching", "RoomQueueMembers->" + roomQueueMembers.get(j).getUserCell()
                    + " Names->" + roomQueueMembers.get(j).getuName());

            Log.e(TAG + " fetching", "Final Storage of Member->" +
                    _roomQueueMembers.get(j).user_name + " Name->" +
                    _roomQueueMembers.get(j).member_id + "\n");
        }
        //return roomQueueMembers;

        if (_queue_member.has("user")) {
            try {
                JSONObject fullName = _queue_member.getJSONObject("fullName");
                String firstName = fullName.getString("firstName");

                Log.e(TAG + " Deep Check", "FirstName->>>" + firstName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            Log.e(TAG + " Deep Check", "Didn't Work!!!");
    }

    public ArrayList<QueueJoinWrapper> getRoomQueueMembers(){
        fetchQueueMembersFromRoom();
        return roomQueueMembers;
    }
}
