package com.ummo.prod.agent.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ummo.prod.agent.R;

public class DetailActivity extends AppCompatActivity {

    private static final String BUNDLE_EXTRAS = "BUNDLE EXTRAS";
    private static final String EXTRA_DETAIL = "EXTRA_DETAIL";
    private static final String EXTRA_ATTR = "EXTRA ATTR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle extras = getIntent().getBundleExtra(BUNDLE_EXTRAS);

        //((TextView)findViewById(R.id.bookerdetails_tv).setText(extras.getString(EXTRA_DETAIL)));
    }
}
