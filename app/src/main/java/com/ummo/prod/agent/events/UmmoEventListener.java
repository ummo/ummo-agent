package com.ummo.prod.agent.events;

/**
 * Created by mosaic on 7/25/16.
 */
public interface UmmoEventListener {
    Object run(Object data);
}

