package com.ummo.prod.agent;

/**
 * Created by mosaic on 5/25/17.
 */


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ummo.prod.agent.ummoAPI.BookingWrapper;
import com.ummo.prod.agent.ummoAPI.interfaces.DataReadyCallBack;

/**
 * Created by mosaic on 4/28/17.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            // Set the alarm here.
            BookingWrapper.setOnLoadedBookings(new DataReadyCallBack() {
                @Override
                public Object exec(Object obj) {
                    for (BookingWrapper wrapper:BookingWrapper.getBookings()) {
                        wrapper.setAlarm();
                    }
                    return null;
                }
            });
        }
    }
}