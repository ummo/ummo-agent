package com.ummo.prod.agent.ummoAPI;

/**
 * Created by mosaic on 5/11/17.
 **/

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.ummoAPI.interfaces.DataReadyCallBack;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class QueueJoinWrapper{
    private String userCell;
    private int position;
    private String numCode;
    private String _id;
    private String joinTime;
    private  String uName;
    private String feedback;
    private int ttdq;
    private static String TAG = "QueueJoinWrapper";
    private static QueueWrapper queue;

    public int getTtdq() {
        return ttdq;
    }

    private void setuName(String uName) {
        this.uName = uName;
    }

    public String getuName() {
        return uName;
    }

    public void setTtdq(int ttdq) {
        this.ttdq = ttdq;
    }

    public QueueWrapper getQueueWrapper() {
        return queue;
    }

    public void setQueueWrapper(QueueWrapper queueWrapper) {
        queue = queueWrapper;
    }

    public static final List<QueueJoinWrapper> qmanQJoins = new ArrayList<>();

    private static Socket socket= UmmoAgent.getApp().getSocket();

    private static DataReadyCallBack callback=null;

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    public static void setDataReadyCallback(DataReadyCallBack _cb){
        callback = _cb;
    }

    public String getUserCell() {
        return userCell;
    }

    public QueueJoinWrapper(JSONObject object){
        try {
            setUserCell(object.getString("userCell"));
            setPosition(object.getInt("possition"));
            setNumCode(object.getString("_id").substring(object.getString("_id").length()-4));
            setJoinTime(object.getString("joinTime"));
            setuName(object.has("user")?object.getJSONObject("user")
                    .getJSONObject("fullName").getString("firstName"):"User");

            //Log.e(TAG+" QUEUE JOIN",object.toString());
            for (int i = 0;i<QueueWrapper.unsent.size();i++){
                if(QueueWrapper.unsent.get(i).getUserCell()
                        .equals(this.getUserCell()))QueueWrapper.unsent.remove(i);
            }
            //Log.e(TAG+" QueueJoinWrapper", " "+object.toString());
        }catch (JSONException jse){
            Log.e("JSON-ERR",jse.toString());
        }
    }

    public static void loadData(){
        //Log.e(TAG+" Loading Data","Queue joins!");

        socket.emit("agent_joins",UmmoAgent.getApp().getUmmoId());
        socket.off("agent_joins");
        socket.on("agent_joins", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                    JSONArray array=(JSONArray)args[0];
                    setData(array);
                    //Log.e(TAG+" Loaded Queue Joins ",array.toString());
            }
        });
    }

    public static void setData(JSONArray array){
        qmanQJoins.clear();
        //Log.e("MUTEX","LOCK");
        //Log.e(TAG+"LOADED JOINS","object.toString()");
        try {
            for (int i=0;i<array.length();i++){
                JSONObject object = array.getJSONObject(i);
                qmanQJoins.add(new QueueJoinWrapper(object));
                //Log.e(TAG+" setData", "JSONArray ->"+array);
            }
            if(callback!=null)
                callback.exec(qmanQJoins);
        }catch (JSONException jse){
            //Log.e(TAG+" setData", "JSON-ERR"+jse.toString());
        }
    }

    public QueueJoinWrapper(String _name, String _cell){
        setuName(_name);
        setUserCell(_cell);
    }

    private void setUserCell(String userCell) {
        this.userCell = userCell;
    }

    public QueueWrapper getQueue() {
        return queue;
    }

    public void setQueue(QueueWrapper queue) {
        QueueJoinWrapper.queue = queue;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getNumCode() {
        return numCode;
    }

    private void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
