package com.ummo.prod.agent.view;

/**
 * Created by José on 23/02/17
 **/

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.adapter.BookingRecAdapter;
import com.ummo.prod.agent.ummoAPI.interfaces.DataReadyCallBack;
import com.ummo.prod.agent.util.RVItemDecoration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.ummo.prod.agent.ummoAPI.BookingWrapper;

public class DiscoverActivity extends AppCompatActivity implements BookingRecAdapter.itemClickCallback{

    private BookingRecAdapter adapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;


    private static final String BUNDLE_EXTRAS = "BUNDLE EXTRAS";
    private static final String EXTRA_DETAIL = "EXTRA_DETAIL";
    private static final String EXTRA_ATTR = "EXTRA ATTR";
    private List<com.ummo.prod.agent.ummoAPI.BookingWrapper> bookingsData = new ArrayList<>();
    SwipeRefreshLayout refreshLayout;

    //private ArrayList bookingsData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookings);
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(this, mixpanelProjectToken);
        swipeRefreshLayout = findViewById(R.id.bookings_refresh_ly);
        //swipeRefreshLayout.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.white));
       // swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.ummo),getResources().getColor(R.color.deep_purple),getResources().getColor(R.color.accent_500));

        setTitle("Manage Subscriptions");
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setSubtitle("Tap on the menu for settings");
        }
        //refreshLayout = (SwipeRefreshLayout) findViewById(R.id.bookings_refresh_ly);
        recyclerView = findViewById(R.id.booking_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(DiscoverActivity.this));
        recyclerView.addItemDecoration(new RVItemDecoration(20));
        swipeRefreshLayout.setRefreshing(false);
        bookingsData.clear();
        //Log.e("WRAPPER",wrapper.get_id());
        bookingsData.addAll(BookingWrapper.getBookings());
        adapter = new BookingRecAdapter(bookingsData, DiscoverActivity.this);
        recyclerView.setAdapter(adapter);
        adapter.setItemClickCallback(DiscoverActivity.this);
        recyclerView.getRecycledViewPool().clear();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper((createHelperCallback()));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        android.support.design.widget.FloatingActionButton fab = findViewById(R.id.bookigFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Mixpanel Tracking of Agent Booking-Mod
                try{
                    JSONObject props = new JSONObject();
                    props.put("fab_bt","clicked");
                    mixpanelAPI.track("Booking Mod",props);
                } catch (JSONException e) {
                    Log.e("Console Activity", "Unable to add properties to JSONObject", e);
                }
                startActivity(new Intent(DiscoverActivity.this,BookingModActivity.class));
            }
        });

        /*if(UmmoAdmin.getApp().getBookingsArray()==null) {
=======

/*<<<<<<< HEAD

        UmmoAgent.getApp().registerEventListener("agent-bookings-ready", new UmmoEventListener() {
                @Override
                public Object run(Object data) {
                    JSONArray array = (JSONArray) data;

                    try {
                        bookingsData.clear();
                        for (int i = 0; i < array.length(); i++) {
                            bookingsData.add(new BookingWrapper(array.getJSONObject(i)));

                            Log.e("DiscoverActivity NULL", bookingsData.toString());
                        }
                    } catch (JSONException jex) {
                        Log.e("BOOKINGS-JAVA", jex.toString());
                    }


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            refreshLayout.setRefreshing(false);
                        }
                    });

                    //adapter.notifyDataSetChanged();
                    return null;
                }
            });

        if(UmmoAgent.getApp().getBookingsArray()==null) {



            // bookingsData =
            // (ArrayList) BookingData.getBookingData();

            assert recyclerView != null;
            recyclerView.setLayoutManager(new LinearLayoutManager(DiscoverActivity.this));
            recyclerView.addItemDecoration(new RVItemDecoration(20));

            final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.bookings_refresh_ly);
            swipeRefreshLayout.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.white));
            swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.ummo), getResources().getColor(R.color.deep_purple), getResources().getColor(R.color.accent_500));
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Toast.makeText(DiscoverActivity.this, "View Refreshed", Toast.LENGTH_SHORT).show();
                    UmmoAgent.getApp().getBookingsFromServer();

                    //Mixpanel Tracking of Agent Sign-Up
                    com.ummo.prod.agent.ummoAPI.BookingWrapper.loadBookings();
                    try {
                        JSONObject props = new JSONObject();
                        props.put("swipe_refresh", "triggered");
                        mixpanelAPI.track("Swipe Refresh", props);
                    } catch (JSONException e) {
                        Log.e("Manage Queue Activity", "Unable to add properties to JSONObject", e);
                    }

                }
            });


            adapter.notifyDataSetChanged();

            ItemTouchHelper itemTouchHelper = new ItemTouchHelper((createHelperCallback()));
            itemTouchHelper.attachToRecyclerView(recyclerView);

            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.e("BOOKINGS", "REFRESH");
                    UmmoAgent.getApp().getBookingsFromServer();
                    BookingWrapper.loadBookings();
                    //UmmoAdmin.getApp().getBookingsFromServer();

                }
            });*/


            com.ummo.prod.agent.ummoAPI.BookingWrapper.setOnLoadedBookings(new DataReadyCallBack() {
                @Override
                public Object exec(Object obj) {
                    bookingsData.clear();
                    bookingsData.addAll(BookingWrapper.getBookings());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                            adapter.notifyDataSetChanged();
                        }
                    });

                    Log.e("BOOKINGSACTIVITY", "Bookings Loaded " + bookingsData.size());
                    return null;

                }
            });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("BOOKINGS","REFRESH");
                BookingWrapper.loadBookings();
                //UmmoAdmin.getApp().getBookingsFromServer();
            }
        });

        com.ummo.prod.agent.ummoAPI.BookingWrapper.setOnLoadedBookings(new DataReadyCallBack() {
            @Override
            public Object exec(Object obj) {
                bookingsData.clear();
                bookingsData.addAll(BookingWrapper.getBookings());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.notifyDataSetChanged();
                    }
                });

                Log.e("BOOKINGSACTIVITY","Bookings Loaded "+bookingsData.size());
                return null;
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.schedule_settings, menu);

        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (android.view.MenuItem item) {
        Log.e("MENU ITEM SELECTED",""+item.getItemId());
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.bmod_settings:
                startActivity(new Intent(this, BookingModActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                //Mixpanel Tracking of Agent Booking Swipe
                final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
                final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), mixpanelProjectToken);
                try{
                    JSONObject props = new JSONObject();
                    props.put("bookingItem_vh","swiped");
                    mixpanelAPI.track("Manage Booking",props);
                } catch (JSONException e) {
                    Log.e("Console Activity", "Unable to add properties to JSONObject", e);
                }
                deleteItem(viewHolder.getAdapterPosition());
                Log.e("POS SWIPED", String.valueOf(viewHolder.getAdapterPosition()));
            }
        };
        return simpleCallback;
    }

    private void moveItem(int oldPos, int newPos) {
        if (oldPos == 0) {
            //JSONObject item =  bookingsData.get(oldPos);
            //bookingsData.remove(oldPos);
           // bookingsData.add(newPos, item);
            //recyclerView.getRecycledViewPool().clear();
            //adapter.notifyItemMoved(oldPos, newPos);
        }
    }

    private void deleteItem(final int pos) {
        Log.e("POS TO BE DELETED", String.valueOf(pos));
        AlertDialog.Builder bookingDoneDialog =
                new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("Are you sure this appointment is done?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(bookingsData==null) Log.e("ON DELETE","List data is null");
                        bookingDone(pos);
                        bookingsData.remove(pos);
                        recyclerView.getRecycledViewPool().clear();
                        adapter.notifyItemRemoved(pos);
                        //CONTINUE FROM HERE
                    }
                })
                .setNegativeButton("No", null).setIcon(R.drawable.ic_action_booking);
        //bookingDoneDialog.show();
        bookingDoneDialog.show().getButton(DialogInterface.BUTTON_POSITIVE)
                .setTextColor(getResources().getColor(R.color.teal));
    }

    @Override
    public void onItemClick(int p) {
//        BookingWrapper item = (BookingWrapper) listData.get(p);

        Intent i = new Intent(this, DetailActivity.class);

        Bundle extras = new Bundle();
       // extras.putString(EXTRA_DETAIL, item.getBooker());
       // extras.putString(EXTRA_ATTR, item.getContactDetails());

        i.putExtra(BUNDLE_EXTRAS, extras);
        //startActivity(i);
    }

    @Override
    public void onSecondaryIconClick(int p) {
      //  BookingWrapper item = (BookingWrapper) listData.get(p);
        /*if (item.isServiced()) {
            item.setServiced(false);
        } else {
            item.setServiced(true);
        }*/

        //adapter.setBookingsData(bookingsData);
        recyclerView.getRecycledViewPool().clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
       // UmmoAdmin.getApp().getBookingsFromServer();
        com.ummo.prod.agent.ummoAPI.BookingWrapper.setOnLoadedBookings(new DataReadyCallBack() {
            @Override
            public Object exec(Object obj) {
                bookingsData.clear();
                for (BookingWrapper wrapper : BookingWrapper.getBookings()) {
                    bookingsData.add(wrapper);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.notifyDataSetChanged();
                    }
                });

                Log.e("BOOKINGSACTIVITY", "Bookings Loaded " + bookingsData.size());
                return null;

            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("BOOKINGS","REFRESH");
                BookingWrapper.loadBookings();
                //UmmoAdmin.getApp().getBookingsFromServer();
            }
        });


        com.ummo.prod.agent.ummoAPI.BookingWrapper.setOnLoadedBookings(new DataReadyCallBack() {
            @Override
            public Object exec(Object obj) {
                bookingsData.clear();
                for(BookingWrapper wrapper:BookingWrapper.getBookings()){
                    bookingsData.add(wrapper);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.notifyDataSetChanged();
                    }
                });

                Log.e("BOOKINGSACTIVITY","Bookings Loaded "+bookingsData.size());
                return null;
            }
        });

    }

    public void bookingDone(int i) {
        bookingsData.get(i).setBookingDone();
    }
}
