package com.ummo.prod.agent.adapter;

/**
 * Created by José on 24/02/17
 **/

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ummo.prod.agent.R;
import com.ummo.prod.agent.ummoAPI.BookingWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BookingRecAdapter extends RecyclerView.Adapter<BookingRecAdapter.BookingHolder> {

    //private List<BookingWrapper> bookingData;
    private LayoutInflater inflater;
    private List<BookingWrapper> bookingsData = new ArrayList<>();

    private itemClickCallback itemClickCallback;


    public interface itemClickCallback {
        void onItemClick(int p);
        void onSecondaryIconClick(int p);
    }

    public void setItemClickCallback (final itemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    public BookingRecAdapter(List<BookingWrapper> bookingData, Context c) {
        this.inflater = LayoutInflater.from(c);
        this.bookingsData = bookingData;
    }

    @Override
    public BookingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.booking_item, parent, false);
        return new BookingHolder(view);
    }

    @Override
    public void onBindViewHolder(BookingHolder holder, int position) {
        if(position>bookingsData.size()-1) return;
        holder.booker.setText(bookingsData.get(position).getName());
        holder.bookedService.setText(bookingsData.get(position).getService());
        holder.contactDetails.setText(bookingsData.get(position).getCell());
        if(bookingsData.get(position).getStart()!=null)holder.bookedDateTime.setText(bookingsData.get(position).getStart().getTime().toString().substring(0,16));
       /* JSONObject item = bookingsData.get(position);
        try {
            if(item.has("booker")){
                holder.booker.setText(item.getJSONObject("booker").getString("name"));
                holder.contactDetails.setText(item.getJSONObject("booker").getString("cell").substring(4,12));
            }
            else {
                holder.booker.setText(item.getJSONObject("user").getString("name"));
                holder.contactDetails.setText(item.getJSONObject("user").getString("cell"));
            }

            holder.bookedService.setText(item.getString("service"));
            String dateTime = item.getString("start");
            Integer month = Integer.valueOf(dateTime.substring(5,7));
            String months[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
            holder.bookedDateTime.setText(dateTime.substring(11,16) + ", " + dateTime.substring(8,10) + " " + months[month-1] + " "+ dateTime.substring(0,4));
        } catch (JSONException jxe){
            Log.e("BookingRecAdapter",jxe.toString()+bookingsData.get(position).toString());
        }*/

        if (position == 0) {
            int here = holder.getAdapterPosition();
            Log.e("REC-VIEW" + here, "!");
            //holder.title.setTextColor(Color.parseColor("#00aaff"));
            //holder.imgViewIcon.setBackgroundResource(R.drawable.ic_circle);
        }

    }

    //public void setBookingsData(List<JSONObject> exerciseList) {
       // this.bookingsData.clear();
        //this.bookingsData.addAll(exerciseList);
   // }

    @Override
    public int getItemCount() {
        return bookingsData.size();
    }

    class BookingHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView booker;
        private TextView contactDetails;
        private TextView bookedService;
        private TextView bookedDateTime;
        private View container;
        private ImageView confirmBtn;
        private ImageView editBtn;
        private ImageView cancelBtn;


        BookingHolder(View itemView) {
            super(itemView);
            booker = itemView.findViewById(R.id.booker_tv);
            contactDetails = itemView.findViewById(R.id.booker_details);
            bookedService = itemView.findViewById(R.id.booker_service);
            bookedDateTime = itemView.findViewById(R.id.booker_date_time);

            confirmBtn = itemView.findViewById(R.id.btn_confirm_booking);
            cancelBtn = itemView.findViewById(R.id.btn_cancel_booking);
            editBtn = itemView.findViewById(R.id.btn_edit_booking);

            container = itemView.findViewById(R.id.container_item_root);
            container.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.container_item_root) {
                itemClickCallback.onItemClick(getAdapterPosition());
            } else {
                itemClickCallback.onSecondaryIconClick(getAdapterPosition());
            }

        }
    }
}