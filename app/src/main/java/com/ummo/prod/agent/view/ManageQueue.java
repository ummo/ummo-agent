package com.ummo.prod.agent.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.daprlabs.cardstack.SwipeDeck;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.ummo.prod.agent.R;
import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.adapter.SwipeDeckAdapter;
import com.ummo.prod.agent.events.UmmoEventListener;
import com.ummo.prod.agent.room.AppDatabase;
import com.ummo.prod.agent.room.entities.Delayed;
import com.ummo.prod.agent.room.entities.Queue;
import com.ummo.prod.agent.room.entities.Served;
import com.ummo.prod.agent.ummoAPI.QMasterListener;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.ummo.prod.agent.ummoAPI.QueueJoinWrapper;
import com.ummo.prod.agent.ummoAPI.QueueWrapper;
import com.ummo.prod.agent.ummoAPI.interfaces.DataReadyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by barnes on 2/7/16.
 **/
public class ManageQueue extends AppCompatActivity implements QMasterListener {
    private SwipeDeck cardStack;
    TextView name;
    TextView alphaNums;

    SharedPreferences sharedPreferences;

    //private DrawerLayout mDrawerLayout;
    //NavigationView mNavigationView;
    FrameLayout mContentFrame;

    private String TAG = "ManageQueue";
    String sharedPrefFile = "com.ummo.prod.agent";
    String FILENAME = "savedQJoins";

    private static final String PREFERENCES_FILE = "mymaterialapp_settings";
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    final ArrayList<QueueJoinWrapper> freshData = new ArrayList<>();
    final ArrayList<QueueJoinWrapper> sharedPrefData = new ArrayList<>();
    final ArrayList<QueueJoinWrapper> roomQueueMembers = new ArrayList<>();
    //final ArrayList<QueueWrapper> reloadedData = new ArrayList<>();

    private Boolean isConnected = UmmoAgent.getApp().isConnected();
    private AppDatabase appDatabase;
    SwipeDeckAdapter adapter;

    @Override
    public void qCreated(String string) {

    }

    @Override
    public void registered(String string) {

    }

    @Override
    public void qDestroyed(String string) {

    }

    @Override
    public void userDQd(String string) {

    }

    @Override
    public void userMoved(String string) {

    }

    @Override
    public void feedBackReceived(String string) {

    }

    @Override
    public void myQReceived(String string) {

    }

    @Override
    public void updatesReceived(String string) {

        Log.e(TAG+" updatesReceived ",string);

        try{
            JSONArray qers= new JSONArray(string);

            //Log.d("Names",qersJSON.names().get(0).toString());
            for(int j=0;j<qers.length();j++) {
                for (int i = 0; i < qers.length(); i++) {
                    if (qers.getJSONObject(i).getInt("position") > freshData.size()) {

                    } else {
                        if(!freshData.contains(qers.getJSONObject(i).toString())) {
                            JSONObject jobject = qers.getJSONObject(i);
                            //jobject.put("id",qersJSON.names().get(i).toString());
                            //freshData.add(jobject.getInt("position"), jobject.toString());
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }

        catch (JSONException jse){
            Log.e(TAG+" updatesReceived",jse.toString());
        }

    }

    //When sheets goes wrong execute the code below
    //I pray it doesn't get to this

    @Override
    public void createQError(String string) {

    }

    @Override
    public void registrationError(String string) {

    }

    @Override
    public void onQDestroyError(String sting) {

    }

    @Override
    public void onUserDQError(String string) {

    }

    @Override
    public void onUserMoveError(String string) {

    }

    @Override
    public void onFeedBackError(String string) {

    }

    @Override
    public void onUpdatesError(String string) {
        Log.e(TAG,string);
    }

    @Override
    protected void onResume() {
        super.onResume();
        QueueJoinWrapper.loadData();

        //Check for internet connectivity
        isInternetOn();
        QueueJoinWrapper.setDataReadyCallback(new DataReadyCallBack() {
            @Override
            public Object exec(Object obj) {
                if(QueueJoinWrapper.qmanQJoins!=null){
                    freshData.clear();
                    freshData.addAll(QueueJoinWrapper.qmanQJoins);

                    Log.e(TAG+ " onResume", "QueueJoin Data->"+QueueJoinWrapper.qmanQJoins);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setupCards();
                        }
                    });
                }
                return null;
            }
        });
    }

    private void reloadCards(){
        QueueJoinWrapper.loadData();

        //Check for internet connectivity
        isInternetOn();
        QueueJoinWrapper.setDataReadyCallback(new DataReadyCallBack() {
            @Override
            public Object exec(Object obj) {
                if(QueueJoinWrapper.qmanQJoins!=null){
                    freshData.clear();
                    freshData.addAll(QueueJoinWrapper.qmanQJoins);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setupCards();
                        }
                    });
                }
                return null;
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initializing AppDatabase
        appDatabase = AppDatabase.getInMemoryDatabase(getApplicationContext());
        insertQueueMembersIntoRoom();
        fetchQueueMembersFromRoom();

        /*for (int i = 0; i<roomQueueMembers.size(); i++){
            String uName = roomQueueMembers.get(i).getJoinTime();
            Log.e(TAG+" onCreate", "Room Pos->"+uName);
        }*/

        setTitle("Manage Queue");
        //Keeping screen on without requiring permissions
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        final ViewGroup parent = (ViewGroup) getWindow().getDecorView().getRootView();

        //Check for internet connectivity
        isInternetOn();

        sharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setSubtitle("Swipe right after servicing");
        }

        QueueJoinWrapper.loadData();

        //TODO
        setupCards();
        //if(UmmoAgent.getApp().isConnected())
        //    Log.e(TAG+" checkConnection", "isConnected ->"+isConnected);

        UmmoAgent.getApp().registerEventListener("disconnected", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                isConnected = false;
                //Log.e(TAG+" Network State->","offline - " +isConnected);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final ForegroundColorSpan redSpan =
                                new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(),
                                        android.R.color.holo_red_light));

                        SpannableStringBuilder snackbarText = new SpannableStringBuilder(getString(R.string.connection_lost));

                        snackbarText.setSpan(redSpan, 0, snackbarText.length(),
                                Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        Snackbar snackbar = Snackbar.make(parent,snackbarText,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.show();
                    }
                });
                return null;
            }
        });

        UmmoAgent.getApp().registerEventListener("onn", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                isConnected = true;
                Log.e(TAG+" Network State->","online - " +isConnected);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final ForegroundColorSpan redSpan =
                                new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(),
                                        R.color.ummo));

                        SpannableStringBuilder snackbarText = new SpannableStringBuilder(getString(R.string.connection_found));

                        snackbarText.setSpan(redSpan, 0, snackbarText.length(),
                                Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        Snackbar snackbar = Snackbar.make(parent,snackbarText, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                });
                return null;
            }
        });

        UmmoAgent.getApp().registerEventListener("qers", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       // setupCards();
                     //   Log.e("MTN ","SUCKS");
                    }
                });
                return null;
            }
        });

        QueueJoinWrapper.setDataReadyCallback(new DataReadyCallBack() {
            @Override
            public Object exec(Object obj) {
                if(QueueJoinWrapper.qmanQJoins!=null){
                    freshData.clear();
                    freshData.addAll(QueueJoinWrapper.qmanQJoins);
                    //Log.e(TAG+" DataReadyCallBack", " freshData"+freshData.toString());
                    SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();

                    preferencesEditor.putString("Q_JOIN_WRAPPER", freshData.toString()).apply();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG+" ","SetupCards");
                            adapter.notifyDataSetChanged();
                            setupCards();
                        }
                    });

                }
                return null;
            }
        });
    }

    private void insertQueueMembersIntoRoom(){
        freshData.clear();
        freshData.addAll(QueueJoinWrapper.qmanQJoins);

        for (int i = 0; i<freshData.size(); i++){
            String _member_id = freshData.get(i).getUserCell();
            String _joinTime = freshData.get(i).getJoinTime();
            String _user_name = freshData.get(i).getuName();
            String _num_code = freshData.get(i).getNumCode();
            int _position = freshData.get(i).getPosition();

            Queue queue = new Queue();
            queue.member_id = _member_id;
            queue.position = _position;
            queue.joinTime = _joinTime;
            queue.user_name = _user_name;
            queue.num_code = _num_code;
            appDatabase.queueDao().insertQueueMember(queue);
            //Log.e(TAG+"", "insertIntoRoom MemberID->"+_member_id+"; MemberPos->"+_position+"; Enq_Time->"+_joinTime);
            Log.e(TAG+"", "insertIntoRoom Queue_Member Entity->"+queue.user_name);
        }
        //After inserting, in case there's a new element/member
        // ...update Room
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e(TAG+" Thread","Err... Couldn't run!",e);
                }
                updateQueueMembersInRoom();
            }
        }).start();

        Log.e(TAG+"", "insertIntoRoom freshData->"+freshData);
    }

    private void updateQueueMembersInRoom(){
        freshData.clear();
        freshData.addAll(QueueJoinWrapper.qmanQJoins);

        for (int i = 0; i<freshData.size(); i++){
            String _member_id = freshData.get(i).getUserCell();
            String _enqueue_time = freshData.get(i).getJoinTime();
            String _user_name = freshData.get(i).getuName();
            String _num_code = freshData.get(i).getNumCode();
            int _position = freshData.get(i).getPosition();

            Queue queue = new Queue();
            queue.member_id = _member_id;
            queue.position = _position;
            queue.joinTime = _enqueue_time;
            queue.user_name = _user_name;
            queue.num_code = _num_code;
            appDatabase.queueDao().updateQueueMember(queue);
            //Log.e(TAG+"", "insertIntoRoom MemberID->"+_member_id+"; MemberPos->"+_position+"; Enq_Time->"+_enqueue_time);
            Log.e(TAG+"", "updatingRoom Queue_Member Entity->"+queue.user_name);
        }

        Log.e(TAG+"", "updatingRoom freshData->"+freshData);
    }

    private void deleteQueueMemberFromRoom(){
        Log.e(TAG+" Dequeue","FreshData->"+freshData.get(0).getUserCell());
        appDatabase.queueDao().dequeueQueueMemberByContact(freshData.get(0).getUserCell());

        Served served = new Served();
        served.user_contact = freshData.get(0).getuName();
        served.joinTime = freshData.get(0).getJoinTime();
        served.served_time = String.valueOf(System.currentTimeMillis());

        appDatabase.servedDao().insertServed(served);

        Log.e(TAG+" serving", "Served User->"+served.user_contact);
        //After deleting/removing a member from Room's Queue,
        //...updating the Room by calling back `insert...`
        updateQueueMembersInRoom();
    }

    private void fetchQueueMembersFromRoom(){
        List<Queue> _roomQueueMembers = appDatabase.queueDao().loadQueueList();

        JSONObject _queue_member = new JSONObject();
        for (int j = 0; j < _roomQueueMembers.size(); j++) {
            Log.e(TAG + " fetching", "Retrieved Members (Room)->" + _roomQueueMembers.get(j).position);
            String _member_id = _roomQueueMembers.get(j).member_id;
            String _user_name = _roomQueueMembers.get(j).user_name;
            Log.e(TAG + " fetching", "Room User-Name->" + _user_name);
            String _num_code = _roomQueueMembers.get(j).num_code;
            int _position = _roomQueueMembers.get(j).position;
            _queue_member = new JSONObject();
            JSONObject user_object = new JSONObject();
            JSONObject full_name = new JSONObject();
            QueueJoinWrapper queue_member;
            try {
                _queue_member.put("userCell", _member_id);
                _queue_member.put("possition", _position);
                _queue_member.put("_id", _num_code);

                full_name.put("firstName", _user_name);
                //full_name.put("surName",_user_name);
                user_object.put("fullName", full_name);
                _queue_member.put("user", user_object);

                Log.e(TAG + " fetching", "Queue_Member->>>" + _queue_member);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG + " fetching", "Err... Error!!!", e);
            }
            queue_member = new QueueJoinWrapper(_queue_member);
            roomQueueMembers.add(queue_member);
            //roomQueueMembers.add(new QueueJoinWrapper(_queue_member));
            Log.e(TAG + " fetching", "RoomQueueMembers->" + roomQueueMembers.get(j).getUserCell()
                    + " Names->" + roomQueueMembers.get(j).getuName());

            Log.e(TAG + " fetching", "Final Storage of Member->" +
                    _roomQueueMembers.get(j).user_name + " Name->" +
                    _roomQueueMembers.get(j).member_id + "\n");
        }
        //return roomQueueMembers;

        if (_queue_member.has("user")) {
            try {
                JSONObject fullName = _queue_member.getJSONObject("fullName");
                String firstName = fullName.getString("firstName");

                Log.e(TAG + "Checking Deeper", "FirstName->>>" + firstName);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG + " CheckingDeeper", "Err... Error->",e);
            }
        } else
            Log.e(TAG + " CheckingDeeper", "Didn't Work!!!");
    }

    public void setupCards(){
        setContentView(R.layout.activity_swipe_deck);
        if(cardStack!=null)
            cardStack.invalidate();

        cardStack = findViewById(R.id.swipe_deck);

        setUpToolbar();

        /*sharedPreferences= getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        String savedQWrapper = sharedPreferences.getString("SAVED_QUEUE_JOINS","");*/

        mContentFrame = findViewById(R.id.nav_contentframe);

        freshData.clear();
        freshData.addAll(QueueJoinWrapper.qmanQJoins);

        Log.e(TAG+" setupCards", "test data -> " + freshData);

        //adapter=new SwipeDeckAdapter(QueueJoinWrapper.qmanQJoins, this);

        /*try {
            JSONArray jsonArray = new JSONArray(savedQWrapper);
            Log.e(TAG+" setupCards", " jsonArray ->"+jsonArray);
            for (int i=0; i<jsonArray.length(); i++){
                JSONObject object = jsonArray.getJSONObject(i);
                //sharedPrefData.add(new QueueJoinWrapper(object));

                //Log.e(TAG+" setupCards", "sharedPrefData ->"+sharedPrefData.get(0).getuName());
            }
        } catch (JSONException jse){
            Log.e(TAG+" setupCards", "JSON-ERR "+jse.toString());
        }*/

        /**
         * Checking for network conditions then setting adapter to use fresh data (freshData)
         * otherwise defaulting to pre-saved data (sharedPrefData)
         * */

        if (isInternetOn()){
            Log.e(TAG+"", "is Internet On -> " + isInternetOn());
            adapter=new SwipeDeckAdapter(freshData,this);
        } else {
            fetchQueueMembersFromRoom();
            Log.e(TAG+"", "is Internet On -> " + isInternetOn());
            adapter = new SwipeDeckAdapter(roomQueueMembers, this);
        }

        for (int x=0; x<freshData.size(); x++){
            String name_1;
            //String name_2;
            name_1 = freshData.get(x).getuName();
            //name_2 = roomQueueMembers.get(x).getuName();
            Log.e(TAG+" setupCards", "FreshData Name->"+name_1);
            //Log.e(TAG+" setupCards", "RoomData Name->"+name_2);
        }

        adapter.notifyDataSetChanged();

        final View view = getCurrentFocus();
        final ViewGroup parent = (ViewGroup) getWindow().getDecorView().getRootView();

        BottomSheetLayout bottomSheet = findViewById(R.id.bottomsheet);

        cardStack.setAdapter(adapter);

        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
                @Override
                public void cardSwipedLeft(int position) {
                    /**
                     * Checking for network conditions before carrying out action
                     * kindly alerting the user that the action will be delayed if
                     * network is unstable; else allowing them to continue gracefully
                     **/
                    if (isInternetOn()) {
                        swipeLeft();
                    }
                    else {
                        AlertDialog.Builder networkAlertDialog =
                                new AlertDialog.Builder(ManageQueue.this)
                                        .setTitle(getString(R.string.action_delay_title))
                                        .setMessage(getString(R.string.action_delay_msg))
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                UmmoAgent.getApp().swipeLeft();
                                                reloadCards();
                                            }
                                        })
                                        .setNegativeButton("Wait", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                reloadCards();
                                            }
                                        })
                                        .setIcon(R.drawable.ic_offline_warning);

                        networkAlertDialog.show().getButton(DialogInterface.BUTTON_POSITIVE)
                                .setTextColor(getResources().getColor(R.color.red));
                    }
                }

                @Override
                public void cardSwipedRight(int position) {
                    /**
                     * Checking for network conditions before carrying out action
                     * kindly alerting the user that the action will be delayed if
                     * network is unstable; else allowing them to continue gracefully
                     **/
                    if (isInternetOn()) {
                        swipeRight();
                    } else {
                        AlertDialog.Builder networkAlertDialog =
                                new AlertDialog.Builder(ManageQueue.this)
                                        .setTitle(getString(R.string.action_delay_title))
                                        .setMessage(getString(R.string.action_delay_msg))
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                QueueWrapper.deQueue();
                                                reloadCards();
                                            }
                                        })
                                        .setNegativeButton("Wait", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                reloadCards();
                                            }
                                        })
                                        .setIcon(R.drawable.ic_offline_warning);

                        networkAlertDialog.show().getButton(DialogInterface.BUTTON_POSITIVE)
                                .setTextColor(getResources().getColor(R.color.red));
                    }
                }

                @Override
                public void cardsDepleted() {
                    final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
                    final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), mixpanelProjectToken);
                    //Mixpanel Tracking of Agent Sign-Up
                    try {
                        JSONObject props = new JSONObject();
                        props.put("queue_cards", "depleted");
                        props.put("queue", "empty");
                        mixpanelAPI.track("Manage Queue", props);
                    } catch (JSONException e) {
                        Log.e(TAG + " cardsDepleted ->", "Unable to add properties to JSONObject", e);
                    }
                    Log.e(TAG + " cardsDepleted ->", "No more cards");
                }

            });

        FloatingActionButton btn = findViewById(R.id.delayFab);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //freshData.add("5");
                cardStack.setAdapter(adapter);
                cardStack.swipeTopCardLeft(380);
                swipeLeft();

            }
        });

        FloatingActionButton btn2 = findViewById(R.id.dequeueFab);
        btn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                cardStack.setAdapter(adapter);
                cardStack.swipeTopCardRight(380);
                swipeRight();
            }
        });

        FloatingActionButton invokeQMod = findViewById(R.id.invokeQMod);
        invokeQMod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
                final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), mixpanelProjectToken);
                //Mixpanel Tracking of Agent Sign-Up
                try{
                    JSONObject props = new JSONObject();
                    props.put("queue_mod_fab","clicked");
                    mixpanelAPI.track("Manage Queue",props);
                } catch (JSONException e) {
                    Log.e(TAG+"", "Unable to add properties to JSONObject", e);
                }
                startActivity(new Intent(ManageQueue.this, ModerateQueue.class));
            }
        });
    }

    private void swipeRight(){
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), mixpanelProjectToken);
        AlertDialog.Builder swipeRightDialog =
                new AlertDialog.Builder(ManageQueue.this)
                        .setTitle("Confirm Service Done?")
                        .setMessage("By 'Swiping Right', you confirm that the customer has been served!")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Dequeue position
                                QueueWrapper.deQueue();
                                deleteQueueMemberFromRoom();
                                insertQueueMembersIntoRoom();
                                //Log.e(TAG+" Dequeue", "QueueDAO->"+)
                                reloadCards();
                                //Mixpanel Tracking of Agent Swipe Right
                                try {
                                    JSONObject props = new JSONObject();
                                    props.put("queue_card", "swiped_right");
                                    mixpanelAPI.track("Manage Queue", props);
                                } catch (JSONException e) {
                                    Log.e(TAG + " swipeRightDialog ->", "Unable to add properties to JSONObject", e);
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                reloadCards();
                            }
                        })
                        .setIcon(R.drawable.ic_manage_queue);

        swipeRightDialog.show().getButton(DialogInterface.BUTTON_POSITIVE)
                .setTextColor(getResources().getColor(R.color.teal));
    }

    private void swipeLeft(){
        final String mixpanelProjectToken = getResources().getString(R.string.mixpanelProjectTokenString);
        final MixpanelAPI mixpanelAPI = MixpanelAPI.getInstance(getApplicationContext(), mixpanelProjectToken);

        AlertDialog.Builder swipeLeftDialog =
                new AlertDialog.Builder(ManageQueue.this)
                        .setTitle("Confirm Service Delay?")
                        .setMessage("By 'Swiping Left', this customer will be taken back!")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UmmoAgent.getApp().swipeLeft();

                                //String delayedUser = roomQueueMembers.get(0).getUserCell();
                                //Log.e(TAG+" Delay", "User delayed->"+delayedUser);

//                                appDatabase.queueDao().delayQueueMember(2, delayedUser);
                                Delayed delayed = new Delayed();
                                delayed.user_contact = freshData.get(0).getuName();
                                delayed.joinTime = freshData.get(0).getJoinTime();
                                delayed.delayed_time = String.valueOf(System.currentTimeMillis());
                                appDatabase.delayedDao().insertDelayed(delayed);
                                Log.e(TAG+" leftSwipe", "Delayed User->"+delayed.user_contact);

                                reloadCards();
                                //Mixpanel Tracking of Agent Sign-Up
                                try {
                                    JSONObject props = new JSONObject();
                                    props.put("queue_card", "swiped_left");
                                    mixpanelAPI.track("Manage Queue", props);
                                } catch (JSONException e) {
                                    Log.e(TAG + " swipeLeftDialog ->", "Unable to add properties to JSONObject", e);
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                reloadCards();
                            }
                        })
                        .setIcon(R.drawable.ic_delay_user);

        swipeLeftDialog.show().getButton(DialogInterface.BUTTON_POSITIVE)
                .setTextColor(getResources().getColor(R.color.teal));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this,QueueListActivity.class));
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public final boolean isInternetOn() {

        final ViewGroup parent = (ViewGroup) getWindow().getDecorView().getRootView();
        //get Connectivity Manager object to check connection
        ConnectivityManager connec = (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        // Check for network connections
        assert connec != null;
        if (    connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED  ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            return true;
        }
        else if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            return false;
        }
        return false;
    }

    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }
    private void setUpToolbar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }
}