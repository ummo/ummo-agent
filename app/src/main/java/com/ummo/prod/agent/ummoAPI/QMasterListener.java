package com.ummo.prod.agent.ummoAPI;

/**
 * Created by barnes on 3/6/16.
 */
public interface QMasterListener
{
    //Success Functions
    void qCreated(String string);
    void registered(String string);
    void qDestroyed(String string);
    void userDQd(String string);
    void userMoved(String string);
    void feedBackReceived(String string);
    void myQReceived(String string);
    void updatesReceived(String string);




    //Error Handling Functions

    void createQError(String string);
    void registrationError(String string);
    void onQDestroyError(String sting);
    void onUserDQError(String string);
    void onUserMoveError(String string);
    void onFeedBackError(String string);
    void onUpdatesError(String string);
}
