package com.ummo.dev.agent;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ummo.prod.agent.R;
import com.ummo.prod.agent.UmmoAgent;
import com.ummo.prod.agent.events.UmmoEventListener;
import com.ummo.prod.agent.view.Console;
import com.ummo.prod.agent.view.SignupActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class SignUpEspressoTest {

    private String testEmail, testCode;
    @Rule
    public ActivityTestRule<SignupActivity> signUpActivityActivityTestRule = new ActivityTestRule<>(SignupActivity.class, true, false);

    @Before
    public void initTestStrings(){
        testEmail = "sakhile@ummo.xyz";//String.valueOf(R.string.esp_signUp_email);
        testCode = "6823";//String.valueOf(R.string.esp_signUp_code);
    }

    @Test
    public void signUp() throws Exception{

        Intent intent = new Intent(String.valueOf(SignupActivity.class));
        signUpActivityActivityTestRule.launchActivity(intent);
        //Enter Agent Email & close soft-keyboard
        onView(withId(R.id.email))
                .perform(typeText(testEmail), closeSoftKeyboard());

        onView(withId(R.id.code))
                .perform(typeText(testCode) , closeSoftKeyboard());

        onView(withId(R.id.btn_login))
                .perform(click());
    }

    @After
    public void completeSignUp() throws Exception{
        UmmoAgent.getApp().signup(testEmail,testCode);
        UmmoAgent.getApp().registerEventListener("signUp", new UmmoEventListener() {
            @Override
            public Object run(Object data) {
                Intent i = new Intent(String.valueOf(Console.class));
                signUpActivityActivityTestRule.launchActivity(i);
                return null;
            }
        });
    }
}