-dontwarn org.w3c.dom.**
-dontwarn org.joda.time.**
-dontwarn org.shaded.apache.**
-dontwarn org.ietf.jgss.**
-dontwarn com.firebase.**
-dontwarn android.support.**
-dontwarn org.slf4j.**
-dontwarn javax.**
-dontwarn com.google.common.**
-dontwarn com.squareup.**
-dontwarn com.mikepenz.**
-dontwarn org.codehaus.**
-dontnote com.firebase.client.core.GaePlatform

#-printseeds#TODO:Note what this does, functionally; as well as other short-key functions

-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes Signature
-keepattributes *Annotation*, InnerClasses, EnclosingMethod, LineNumberTable, SourceFile

-keep class com.ummo.prod.user.** { *; }
-keep class cn.pedant.SweetAlert.** { *; }
#Not recommended
#-keep class !com.ummo.prod.user.** { *; }

# Basic ProGuard rules for Firebase Android SDK 2.0.0+
-keep class com.firebase.** { *; }
#Other Google Libraries
#-keep class com.google.android.** {*;}
-keep class com.google.** {*;}
-keep class com.google.common.util.** {*;}

-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }

#Processing Butterknife code:
#   Preserves the Butterknife annotations, annotated fields & methods as well as
#   generated classes & methods that Butterknife accesses by reflection.
-keep @interface butterknife.*

-keepclasseswithmembers class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembers class * {
    @butterknife.* <methods>;
}

-keepclasseswithmembers class * {
    @butterknife.On* <methods>;
}

-keep class **$$ViewInjector {
    public static void inject(...);
    public static void reset(...);
}

-keep class **$$ViewBinder {
    public static void bind(...);
    public static void unbind(...);
}
#Removing logging code
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}
